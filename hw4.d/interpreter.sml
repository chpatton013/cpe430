use "parser.sml";

fun failType (opr:string) (req:string) (found:string) =
  (
    print ("operator '" ^ opr ^ "' requires " ^ req ^ ", found " ^ found);
    OS.Process.exit (OS.Process.failure);
    EXP_FALSE
  )
;

fun failMapping (str:string) =
  case str of
    "number" => "int"
  | "boolean" => "bool"
  | _ => str
;

fun bopToString (BOP_PLUS) = "+"
  | bopToString (BOP_MINUS) = "-"
  | bopToString (BOP_TIMES) = "*"
  | bopToString (BOP_DIVIDE) = "/"
  | bopToString (BOP_MOD) = "%"
  | bopToString (BOP_EQ) = "=="
  | bopToString (BOP_NE) = "!="
  | bopToString (BOP_LT) = "<"
  | bopToString (BOP_GT) = ">"
  | bopToString (BOP_LE) = "<="
  | bopToString (BOP_GE) = ">="
  | bopToString (BOP_AND) = "&&"
  | bopToString (BOP_OR) = "||"
  | bopToString (BOP_COMMA) = ","
;

fun uopToString (UOP_NOT) = "!"
  | uopToString (UOP_TYPEOF) = "typeof "
  | uopToString (UOP_MINUS) = "-"
;

fun exprToString (EXP_NUM n) =
    if (n < 0) then ("-" ^ (Int.toString (Int.abs n))) else (Int.toString n)
  | exprToString (EXP_STRING str) = "\"" ^ str ^ "\""
  | exprToString (EXP_TRUE) = "true"
  | exprToString (EXP_FALSE) = "false"
  | exprToString (EXP_UNDEFINED) = "undefined"
  | exprToString (expression) =
    (
      failType "" "";
      ""
    )
;

fun printExpression (EXP_BINARY binExp) =
    (
      print "(";
      printExpression (#lft binExp);
      print (" " ^ (bopToString (#opr binExp)) ^ " ");
      printExpression (#rht binExp);
      print ")"
    )
  | printExpression (EXP_UNARY unExp) =
    (
      print "(";
      print (uopToString (#opr unExp));
      printExpression (#opnd unExp);
      print ")"
    )
  | printExpression (EXP_COND condExp) =
    (
      print "(";
      printExpression (#guard condExp);
      print " ? ";
      printExpression (#thenExp condExp);
      print " : ";
      printExpression (#elseExp condExp);
      print ")"
    )
  | printExpression (expr:expression) = print (exprToString expr)
;

fun printStatement (ST_EXP stExp) =
  printExpression (#exp stExp)
;

fun printSourceElement (STMT stmt) =
  (
    printStatement (#stmt stmt);
    print ";\n"
  )
;



fun expStringToString (EXP_STRING str) =
  str

and expToBool (EXP_NUM n) = if (n = 0) then EXP_FALSE else EXP_TRUE
  | expToBool (EXP_STRING str) = if (str = "") then EXP_FALSE else EXP_TRUE
  | expToBool (EXP_TRUE) = EXP_TRUE
  | expToBool (EXP_FALSE) = EXP_FALSE
  | expToBool (EXP_UNDEFINED) = EXP_FALSE
  | expToBool (expr) = expToBool (evalExpression expr)

and evalUnaryNot (EXP_TRUE) = EXP_FALSE
  | evalUnaryNot (EXP_FALSE) = EXP_TRUE
  | evalUnaryNot (EXP_NUM _) =
    (
      print ("unary operator '!' requires bool, found int");
      OS.Process.exit (OS.Process.failure);
      EXP_FALSE
    )
  | evalUnaryNot (EXP_STRING _) =
    (
      print ("unary operator '!' requires bool, found string");
      OS.Process.exit (OS.Process.failure);
      EXP_FALSE
    )
  | evalUnaryNot (EXP_UNDEFINED) =
    (
      print ("unary operator '!' requires bool, found undefined");
      OS.Process.exit (OS.Process.failure);
      EXP_FALSE
    )
  | evalUnaryNot (expr) = evalUnaryNot (expToBool expr)

and evalUnaryTypeof (EXP_NUM n) = EXP_STRING "number"
  | evalUnaryTypeof (EXP_STRING str) = EXP_STRING "string"
  | evalUnaryTypeof (EXP_TRUE) = EXP_STRING "boolean"
  | evalUnaryTypeof (EXP_FALSE) = EXP_STRING "boolean"
  | evalUnaryTypeof (EXP_UNDEFINED) = EXP_STRING "undefined"

and evalUnaryMinus (EXP_NUM n) = EXP_NUM (~n)
  | evalUnaryMinus (expr:expression) =
    (
      print ("unary operator '-' requires int, found " ^
        (failMapping (expStringToString (
          (evalUnaryExpression UOP_TYPEOF (evalExpression expr)))
        ))
      );
      OS.Process.exit (OS.Process.failure);
      EXP_FALSE
    )

and evalUnaryExpression (UOP_NOT) (expr) = evalUnaryNot expr
  | evalUnaryExpression (UOP_TYPEOF) (expr) = evalUnaryTypeof expr
  | evalUnaryExpression (UOP_MINUS) (expr) = evalUnaryMinus expr

and evalNum (BOP_PLUS) (EXP_NUM n1) (EXP_NUM n2) = EXP_NUM (n1 + n2)
  | evalNum (BOP_PLUS) (EXP_STRING str1) (EXP_STRING str2) =
    EXP_STRING (str1 ^ str2)
  | evalNum (BOP_MINUS) (EXP_NUM n1) (EXP_NUM n2) = EXP_NUM (n1 - n2)
  | evalNum (BOP_TIMES) (EXP_NUM n1) (EXP_NUM n2) = EXP_NUM (n1 * n2)
  | evalNum (BOP_DIVIDE) (EXP_NUM n1) (EXP_NUM n2) = EXP_NUM (n1 div n2)
  | evalNum (BOP_MOD) (EXP_NUM n1) (EXP_NUM n2) = EXP_NUM (n1 mod n2)
  | evalNum (BOP_LT) (EXP_NUM n1) (EXP_NUM n2) =
    if (n1 < n2) then EXP_TRUE else EXP_FALSE
  | evalNum (BOP_GT) (EXP_NUM n1) (EXP_NUM n2) =
    if (n1 > n2) then EXP_TRUE else EXP_FALSE
  | evalNum (BOP_LE) (EXP_NUM n1) (EXP_NUM n2) =
    if (n1 <= n2) then EXP_TRUE else EXP_FALSE
  | evalNum (BOP_GE) (EXP_NUM n1) (EXP_NUM n2) =
    if (n1 >= n2) then EXP_TRUE else EXP_FALSE
  | evalNum (bop:binaryOperator) (expr1:expression) (expr2:expression) =
    failType (bopToString bop) (
      if (bop = BOP_PLUS)
      then ("int * int or string * string")
      else ("int * int")
    )
    (
      (failMapping (expStringToString (
        evalUnaryExpression UOP_TYPEOF (evalExpression expr1))
      )) ^
      " * " ^
      (failMapping (expStringToString (
        (evalUnaryExpression UOP_TYPEOF (evalExpression expr2)))
      ))
    )

and evalNumOperation (bop:binaryOperator) (lft:expression) (rht:expression) =
  let
    val lftVal = evalExpression lft
    val rhtVal = evalExpression rht
  in
    evalNum bop lftVal rhtVal
  end

and evalBinaryExpression (BOP_PLUS) (lft:expression) (rht:expression) =
    evalNumOperation BOP_PLUS lft rht
  | evalBinaryExpression (BOP_MINUS) (lft:expression) (rht:expression) =
    evalNumOperation BOP_MINUS lft rht
  | evalBinaryExpression (BOP_TIMES) (lft:expression) (rht:expression) =
    evalNumOperation BOP_TIMES lft rht
  | evalBinaryExpression (BOP_DIVIDE) (lft:expression) (rht:expression) =
    evalNumOperation BOP_DIVIDE lft rht
  | evalBinaryExpression (BOP_MOD) (lft:expression) (rht:expression) =
    evalNumOperation BOP_MOD lft rht
  | evalBinaryExpression (BOP_LT) (lft:expression) (rht:expression) =
    evalNumOperation BOP_LT lft rht
  | evalBinaryExpression (BOP_GT) (lft:expression) (rht:expression) =
    evalNumOperation BOP_GT lft rht
  | evalBinaryExpression (BOP_LE) (lft:expression) (rht:expression) =
    evalNumOperation BOP_LE lft rht
  | evalBinaryExpression (BOP_GE) (lft:expression) (rht:expression) =
    evalNumOperation BOP_GE lft rht
  | evalBinaryExpression (BOP_EQ) (lft:expression) (rht:expression) =
    if ((evalExpression lft) = (evalExpression rht))
    then (EXP_TRUE)
    else (EXP_FALSE)
  | evalBinaryExpression (BOP_NE) (lft:expression) (rht:expression) =
    if ((evalExpression lft) = (evalExpression rht))
    then (EXP_FALSE)
    else (EXP_TRUE)
  | evalBinaryExpression (BOP_AND) (lft:expression) (rht:expression) =
    let
      val lftVal = evalExpression lft
    in
      if (lftVal = EXP_TRUE)
      then (
        let
          val rhtVal = evalExpression rht
        in
          if (rhtVal = EXP_TRUE)
          then (EXP_TRUE)
          else (
            if (rhtVal = EXP_FALSE)
            then (EXP_FALSE)
            else (
              failType (bopToString BOP_AND)
              ("bool * bool")
              (
                (failMapping (expStringToString (
                  evalUnaryExpression UOP_TYPEOF (evalExpression lft))
                )) ^
                " * " ^
                (failMapping (expStringToString (
                  (evalUnaryExpression UOP_TYPEOF (evalExpression rht)))
                ))
              )
            )
          )
        end
      )
      else (
        if (lftVal = EXP_FALSE)
        then (EXP_FALSE)
        else (
          failType (bopToString BOP_AND)
          ("bool")
          (failMapping (expStringToString
            (evalUnaryExpression UOP_TYPEOF (evalExpression lft))
          ))
        )
      )
    end
  | evalBinaryExpression (BOP_OR) (lft:expression) (rht:expression) =
    let
      val lftVal = evalExpression lft
    in
      if (lftVal = EXP_TRUE)
      then (EXP_TRUE)
      else (
        if (lftVal = EXP_FALSE)
        then (
          let
            val rhtVal = evalExpression rht
          in
            if (rhtVal = EXP_TRUE)
            then (EXP_TRUE)
            else (
              if (rhtVal = EXP_FALSE)
              then (EXP_FALSE)
              else (
                failType (bopToString BOP_OR)
                ("bool * bool")
                (
                  (failMapping (expStringToString (
                    evalUnaryExpression UOP_TYPEOF (evalExpression lft))
                  )) ^
                  " * " ^
                  (failMapping (expStringToString (
                    (evalUnaryExpression UOP_TYPEOF (evalExpression rht)))
                  ))
                )
              )
            )
          end
        )
        else (
          failType (bopToString BOP_OR)
          ("bool")
          (failMapping (expStringToString (
            evalUnaryExpression UOP_TYPEOF (evalExpression lft))
          ))
        )
      )
    end
  | evalBinaryExpression (BOP_COMMA) (lft:expression) (rht:expression) =
    (
      evalExpression lft;
      evalExpression rht
    )

and evalExpression (EXP_BINARY exBin) =
    evalBinaryExpression (#opr exBin) (#lft exBin) (#rht exBin)
  | evalExpression (EXP_UNARY exUn) =
    evalUnaryExpression (#opr exUn) (#opnd exUn)
  | evalExpression (EXP_COND exCond) =
    let
      val guard = evalExpression (#guard exCond)
    in
      if (guard = EXP_TRUE)
      then (evalExpression (#thenExp exCond))
      else (
        if (guard = EXP_FALSE)
        then (evalExpression (#elseExp exCond))
        else (
          (
            print ("boolean guard required for 'cond' expression, found" ^
              (failMapping (expStringToString (
                (evalUnaryExpression UOP_TYPEOF (evalExpression guard)))
              ))
            );
            OS.Process.exit (OS.Process.failure);
            EXP_FALSE
          )
        )
      )
    end
  | evalExpression (expr:expression) = expr
;

fun evalStatement (ST_EXP stExp) =
  evalExpression (#exp stExp)
;

fun evalSourceElement (STMT stmt) =
  let
    val value = evalStatement (#stmt stmt);
  in
    printStatement (#stmt stmt);
    print (" ==> " ^ (exprToString value) ^ "\n")
  end
;

fun evalSourceElementList ([]) =
    ()
  | evalSourceElementList ((x::xs):sourceElement list) =
    (
      evalSourceElement x;
      evalSourceElementList xs
    )
;

fun evaluate (PROGRAM pgm) =
  evalSourceElementList (#elems pgm)
;

fun interpret (file:string) =
  evaluate (parse file)
;
