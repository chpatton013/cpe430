use "parser.sml";

fun out s = TextIO.output (TextIO.stdOut, s);

fun printList print_single delim (x::(xs::xss)) =
   (print_single x; out delim; printList print_single delim (xs::xss))
  | printList print_single delim (x::xs) = (print_single x)
  | printList print_single delim [] = ()

fun binaryOperatorString BOP_PLUS = "+"
  | binaryOperatorString BOP_MINUS = "-"
  | binaryOperatorString BOP_TIMES = "*"
  | binaryOperatorString BOP_DIVIDE = "/"
  | binaryOperatorString BOP_MOD = "%"
  | binaryOperatorString BOP_EQ = "=="
  | binaryOperatorString BOP_NE = "!="
  | binaryOperatorString BOP_LT = "<"
  | binaryOperatorString BOP_GT = ">"
  | binaryOperatorString BOP_LE = "<="
  | binaryOperatorString BOP_GE = ">="
  | binaryOperatorString BOP_AND = "&&"
  | binaryOperatorString BOP_OR = "||"
  | binaryOperatorString BOP_COMMA = ","
;

fun unaryOperatorString UOP_NOT = "!"
  | unaryOperatorString UOP_TYPEOF = "typeof "
  | unaryOperatorString UOP_MINUS = "-"
;

fun printParameterList params =
   (out "(";
    printList printExpression ", " params;
    out ")"
   )
and printFunction (EXP_FUNCTION {binding, params, body}) is_declaration =
   ((if is_declaration then () else out "(");
    out "function ";
    (if isSome binding
    then printExpression (valOf binding)
    else ());
    printParameterList params;
    out "\n{\n";
    printList printSourceElement "" body;
    out "}\n";
    (if is_declaration then () else out ")")
   )
  | printFunction _ is_declaration = error "unimplemented"

and printExpression (EXP_ID s) = out s
  | printExpression (EXP_NUM n) =
   out (if n < 0 then "-" ^ (Int.toString (~n)) else Int.toString n)
  | printExpression (EXP_STRING s) = out ("\"" ^ (String.toString s) ^ "\"")
  | printExpression EXP_TRUE = out "true"
  | printExpression EXP_FALSE = out "false"
  | printExpression EXP_UNDEFINED = out "undefined"
  | printExpression (EXP_BINARY {opr, lft, rht}) =
   (out "(";
    printExpression lft;
    out " ";
    out (binaryOperatorString opr);
    out " ";
    printExpression rht;
    out ")"
   )
  | printExpression (EXP_UNARY {opr, opnd}) =
   (out "(";
    out (unaryOperatorString opr);
    printExpression opnd;
    out ")"
   )
  | printExpression (EXP_COND {guard, thenExp, elseExp}) =
   (out "(";
    printExpression guard;
    out " ? ";
    printExpression thenExp;
    out " : ";
    printExpression elseExp;
    out ")"
   )
  | printExpression (EXP_ASSIGN {lhs, rhs}) =
   (out "(";
    printExpression lhs;
    out " = ";
    printExpression rhs;
    out ")"
   )
  | printExpression (EXP_VAR {binding, eval}) =
   (printExpression binding;
    case eval of
       NONE => ()
    |  SOME x => (out " = "; printExpression x)
   )
  | printExpression (func as EXP_FUNCTION {binding, params, body}) =
   printFunction func false
  | printExpression (EXP_CALL {mem, args}) =
   (printExpression mem;
    printParameterList args
   )

and printStatement (ST_EXP {exp}) =
   (printExpression exp; out ";\n")
  | printStatement (ST_BLOCK {stmts}) =
   (out "{\n";
    List.app printStatement stmts;
    out "}\n"
   )
  | printStatement (ST_IF {guard, th, el}) =
   (out "if (";
    printExpression guard;
    out ")\n";
    printStatement th;
    out "else\n";
    printStatement el
   )
  | printStatement (ST_PRINT {exp}) =
   (out "print ";
    printExpression exp;
    out ";\n"
   )
  | printStatement (ST_WHILE {guard, body}) =
   (out "while (";
    printExpression guard;
    out ")\n";
    printStatement body
   )
  | printStatement (ST_VAR {vars}) =
   (out "var ";
    printList printExpression ", " vars;
    out ";\n"
   )
  | printStatement (ST_RETURN {exp}) =
   (out "return";
    (case exp of
        NONE => ()
     |  SOME x => (out " "; printExpression x)
    );
    out ";\n"
   )

and printSourceElement (STMT {stmt}) = printStatement stmt
  | printSourceElement (FUNC_DECL {func}) = printFunction func true

and printSourceElements [] = ()
  | printSourceElements (el::els) =
   (printSourceElement el; printSourceElements els)

and printAST (PROGRAM {elems}) =
   printList printSourceElement "" elems
;
