(* Part 1 *)
fun intToString n =
   if n < 0
   then "-" ^ (Int.toString (~n))
   else Int.toString n
;

(* Part 2 *)
fun listLength [] = 0
  | listLength (_::xs) = 1 + listLength xs
;
(* Or,
fun listLength L =
   foldl (fn (x, res) => 1 + res) 0 L
;
*)

(* Part 3 *)
fun numberOf v [] = 0
  | numberOf v (x::xs) =
   if v = x
   then 1 + (numberOf v xs)
   else (numberOf v xs)
;

(* Or,
fun numberOf v L =
   foldl (fn (x, res) => (if v = x then 1 else 0) + res) 0 L
;
*)

(* Part 4 *)
fun pairSwap [] = []
  | pairSwap ((a,b)::xs) = (b,a)::(pairSwap xs)
;

(* Or, 
fun pairSwap L = map (fn (a,b) => (b,a)) L;
*)

(* Part 5 *)
fun findPair key [] = NONE
  | findPair key ((k, v)::xs) =
   if key = k
   then SOME (k, v)
   else findPair key xs
;

(* Or,
fun findPair key L = List.find (fn (k, v) => k = key) L;
*)

(* Part 6 *)
fun unzip [] = ([], [])
  | unzip ((x,y)::zs) =
   let
      val (xs, ys) = unzip zs;
   in
      (x::xs, y::ys)
   end
;

(* Or,
fun unzip L = foldr (fn ((x,y), (xs,ys)) => (x::xs, y::ys)) ([], []) L;
*)

(* Part 7 *)
exception UnbalancedZip;

fun zip [] [] = []
  | zip [] (y::ys) = raise UnbalancedZip
  | zip (x::xs) [] = raise UnbalancedZip
  | zip (x::xs) (y::ys) = (x,y)::(zip xs ys)
;

(* Part 8 *)
fun splitFilter func [] = ([], [])
  | splitFilter func (z::zs) =
   let
      val (xs, ys) = splitFilter func zs;
   in
      if func z
      then (z::xs, ys)
      else (xs, z::ys)
   end
;

(* Or,
fun splitFilter func L =
   foldr (fn (z, (xs,ys)) =>
         if func z
         then (z::xs, ys)
         else (xs, z::ys)
      ) ([], []) L;
*)

(* Part 9 *)
fun charSubst L c =
   case List.find (fn (a, b) => c = a) L of
      NONE => c
   |  SOME (a,b) => b
;

fun handleCharSubst fstr pairs =
   case (TextIO.input1 fstr) of
      SOME x => (TextIO.print (str (charSubst pairs x)))
    | NONE => ()
;

fun echoSubst fstr pairs =
   if TextIO.endOfStream fstr
   then ()
   else (handleCharSubst fstr pairs; echoSubst fstr  pairs)
;

fun fileSubst file pairs =
   let
      val fstr = TextIO.openIn file
   in
      echoSubst fstr pairs
   end
;

(* Part 10 *)
fun isSpaceNext fstr =
   case TextIO.lookahead fstr of
     SOME c => Char.isSpace c
   | NONE => false
;

fun skipSpaces fstr =
   if isSpaceNext fstr
   then (TextIO.input1 fstr; skipSpaces fstr)
   else ()
;

fun addNextChar fstr L =
   case TextIO.input1 fstr of
     SOME c => c::L
   | NONE => L
; 

fun addWord "" L = L
  | addWord s L = s::L
;

fun buildSingleWord fstr L wordL =
   if (TextIO.endOfStream fstr) orelse (isSpaceNext fstr)
   then addWord (implode (rev wordL)) L
   else buildSingleWord fstr L (addNextChar fstr wordL)
;
   
fun gatherWords fstr L =
   if TextIO.endOfStream fstr
   then L
   else gatherWords fstr (skipSpaces fstr; buildSingleWord fstr L [])
;

fun wordsFromFile file =
   let
      val fstr = TextIO.openIn file
   in
      rev (gatherWords fstr [])
   end
;
