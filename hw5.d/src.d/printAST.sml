use "parser.sml";

fun out s = TextIO.output (TextIO.stdOut, s);

fun binaryOperatorString BOP_PLUS = "+"
  | binaryOperatorString BOP_MINUS = "-"
  | binaryOperatorString BOP_TIMES = "*"
  | binaryOperatorString BOP_DIVIDE = "/"
  | binaryOperatorString BOP_MOD = "%"
  | binaryOperatorString BOP_EQ = "=="
  | binaryOperatorString BOP_NE = "!="
  | binaryOperatorString BOP_LT = "<"
  | binaryOperatorString BOP_GT = ">"
  | binaryOperatorString BOP_LE = "<="
  | binaryOperatorString BOP_GE = ">="
  | binaryOperatorString BOP_AND = "&&"
  | binaryOperatorString BOP_OR = "||"
  | binaryOperatorString BOP_COMMA = ","
  | binaryOperatorString BOP_ASSIGN = "="
;

fun unaryOperatorString UOP_NOT = "!"
  | unaryOperatorString UOP_TYPEOF = "typeof "
  | unaryOperatorString UOP_MINUS = "-"
;

fun printExpression (EXP_NUM n) =
   out (if n < 0 then "-" ^ (Int.toString (~n)) else Int.toString n)
  | printExpression (EXP_STRING s) =
   out ("\"" ^ (String.toString s) ^ "\"")
  | printExpression (EXP_ID id) =
   out (String.toString id)
  | printExpression EXP_TRUE =
   out "true"
  | printExpression EXP_FALSE =
   out "false"
  | printExpression EXP_UNDEFINED =
   out "undefined"
  | printExpression (EXP_BINARY {opr, lft, rht}) =
   (out "(";
    printExpression lft;
    out " ";
    out (binaryOperatorString opr);
    out " ";
    printExpression rht;
    out ")"
   )
  | printExpression (EXP_UNARY {opr, opnd}) =
   (out "(";
    out (unaryOperatorString opr);
    printExpression opnd;
    out ")"
   )
  | printExpression (EXP_COND {guard, thenExp, elseExp}) =
   (out "(";
    printExpression guard;
    out " ? ";
    printExpression thenExp;
    out " : ";
    printExpression elseExp;
    out ")"
   )
;

fun printStatement (ST_EXP {exp}) =
      (printExpression exp; out ";\n")
  | printStatement (ST_BLOCK {stmts}) =
      (out "{\n"; printStatements stmts; out "}\n")
  | printStatement (ST_IF {guard, thenBlock, elseBlock}) =
      (
         out "if ("; printExpression guard; out ")\n";
         printStatement thenBlock;
         out "else\n";
         printStatement elseBlock
      )
  | printStatement (ST_PRINT {exp}) =
      (out "print "; printExpression exp; out ";\n")
  | printStatement (ST_ITERATION {guard, block}) =
      (
         out "while ("; printExpression guard; out ")\n";
         printStatement block
      )
and printStatements [] = ()
  | printStatements (x::xs) =
      (printStatement x; printStatements xs)
;

fun printSourceElement (STMT {stmt}) =
   printStatement stmt
;

fun printSourceElements [] = ()
  | printSourceElements (el::els) =
   (printSourceElement el; printSourceElements els)
;

fun printAST (PROGRAM {elems}) =
   printSourceElements elems
;
