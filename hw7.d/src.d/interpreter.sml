use "printAST.sml";
use "map.sml";

exception MalformedEnvironment;

datatype value =
     Num_Value of int
   | String_Value of string
   | Bool_Value of bool
   | Undefined_Value
   | Closure_Value of {params: string list, body: sourceElement list,
      env: (string, value) HashTable.hash_table list, rf: unit ref}
   | Object_Value of {properties: (string, value) HashTable.hash_table}
;

fun valueToString (Num_Value n) =
   (if n < 0 then "-" ^ (Int.toString (~n)) else Int.toString n)
  | valueToString (String_Value s) = s
  | valueToString (Bool_Value b) = Bool.toString b
  | valueToString Undefined_Value = "undefined"
  | valueToString (Closure_Value _) = "function"
  | valueToString (Object_Value _) = "object"
;

fun typeString (Num_Value _) = "number"
  | typeString (Bool_Value _) = "boolean"
  | typeString (String_Value _) = "string"
  | typeString (Undefined_Value) = "undefined"
  | typeString (Closure_Value _) = "function"
  | typeString (Object_Value _) = "object"
;

fun propertyTypeError found =
   error ("field reference '.' requires object, found " ^
    (typeString found) ^ "\n")
;

fun condTypeError found =
   error ("boolean guard required for 'cond' expression, found " ^
      (typeString found) ^ "\n")
;

fun unaryTypeError expected found oper =
   error ("unary operator '" ^
      (unaryOperatorString oper) ^ "' requires " ^
      (typeString expected) ^ ", found " ^ (typeString found) ^ "\n")
;

fun boolTypeError found oper =
   error ("operator '" ^ (binaryOperatorString oper) ^
      "' requires " ^ (typeString (Bool_Value true)) ^
      ", found " ^ (typeString found) ^ "\n")
;

fun binaryTypeError elft erht flft frht oper =
   error ("operator '" ^ (binaryOperatorString oper) ^ "' requires " ^
      (typeString elft) ^ " * " ^ (typeString erht) ^ ", found " ^
      (typeString flft) ^ " * " ^ (typeString frht) ^ "\n")
;

fun addTypeError flft frht oper =
   error ("operator '" ^ (binaryOperatorString oper) ^ "' requires " ^
      (typeString (Num_Value 0)) ^ " * " ^
      (typeString (Num_Value 0)) ^ " or " ^
      (typeString (String_Value "")) ^ " * " ^
      (typeString (String_Value "")) ^
      ", found " ^ (typeString flft) ^ " * " ^ (typeString frht) ^ "\n")
;

fun ifTypeError found =
   error ("boolean guard required for 'if' statement, found " ^
      (typeString found) ^ "\n")
;

fun whileTypeError found =
   error ("boolean guard required for 'while' statement, found " ^
      (typeString found) ^ "\n")
;

fun valuePairEquals (v1, v2) = valueEquals v1 v2
and valueEquals (v1 as Num_Value _) v2 = numEquals v1 v2
  | valueEquals (v1 as String_Value _) v2 = stringEquals v1 v2
  | valueEquals (v1 as Bool_Value _) v2 = boolEquals v1 v2
  | valueEquals (v1 as Undefined_Value) v2 = undefinedEquals v1 v2
  | valueEquals (v1 as Closure_Value _) v2 = closureEquals v1 v2
  | valueEquals (v1 as Object_Value _) v2 = objectEquals v1 v2
and numEquals (Num_Value n1) (Num_Value n2) = n1 = n2
  | numEquals (Num_Value _) _ = false
and stringEquals (String_Value s1) (String_Value s2) = s1 = s2
  | stringEquals (String_Value _) _ = false
and boolEquals (Bool_Value b1) (Bool_Value b2) = b1 = b2
  | boolEquals (Bool_Value _) _ = false
and undefinedEquals (Undefined_Value) (Undefined_Value) = true
  | undefinedEquals (Undefined_Value) _ = false
and closureEquals (Closure_Value c1) (Closure_Value c2) = #rf c1 = #rf c2
  | closureEquals (Closure_Value _) _ = false
and objectEquals (Object_Value obj1) (Object_Value obj2) =
   let
      val l1 = HashTable.listItems (#properties obj1)
      val l2 = HashTable.listItems (#properties obj2)
   in
      if List.length l1 = List.length l2
      then ListPair.all valuePairEquals (l1, l2)
      else false
   end
  | objectEquals (Object_Value _) _ = false
;

fun createClosureValue params body env =
   Closure_Value {params=params, body=body, env=env, rf=ref ()}
;

fun objectProtoSearch (obj as Object_Value {properties}) func =
   let val proto = HashTable.find properties "[[prototype]]"
   in (case proto of
          NONE => func properties
       |  SOME x => (
             case func properties of
                NONE => objectProtoSearch x func
             |  rtn => rtn
          )
      )
   end
  | objectProtoSearch v func = NONE
;
fun objectGetProperty obj id =
   (case objectProtoSearch obj (fn props => HashTable.find props id) of
       SOME x => x
    |  NONE => Undefined_Value
   )
;
fun objectSetProperty (obj as Object_Value {properties}) id v =
   if contains properties id
   then valOf (objectProtoSearch obj (fn props =>
            if contains props id
            then (insert props id v; SOME v)
            else NONE
         )
   )
   else (insert properties id v; v)
  | objectSetProperty v _ _ = propertyTypeError v
;

fun addThisToClosure (Closure_Value closure) (obj as Object_Value _) =
   insert (hd (#env closure)) "this" obj;
;
fun createObjectValue props proto =
   let val rtn = Object_Value {properties=(new_map ())}
   in (
      objectSetProperty rtn "[[prototype]]" proto;
      List.app
       (fn (id, v) => (
          (case v of
              Closure_Value _ => (addThisToClosure v rtn; ())
           |  _ => ()
          );
          objectSetProperty rtn id v;
          ()
       ))
       props;
      rtn
   )
   end
;

fun envChain {chain, retVal} = chain;
fun envRetVal {chain, retVal} = retVal;
fun createEnv chain retVal = {chain=chain, retVal=retVal};

fun atTopLevel {chain, retVal} = (length chain) = 1;

fun insertCurrent env id v = (insert (hd (envChain env)) id v; env);
fun declareCurrent env id =
   if contains (hd (envChain env)) id
   then env
   else (insert (hd (envChain env)) id Undefined_Value; env)
;
fun insertEnvH [] id v = raise MalformedEnvironment
  | insertEnvH [global] id v = insert global id v
  | insertEnvH (env::envs) id v =
   if contains env id
   then insert env id v
   else insertEnvH envs id v
;
fun insertEnv env id v = (insertEnvH (envChain env) id v; env);
fun lookupEnvH [] id = Undefined_Value
  | lookupEnvH (env::envs) id =
   if contains env id
   then lookup env id
   else lookupEnvH envs id
;
fun lookupEnv env id = lookupEnvH (envChain env) id;
fun growEnvironment env =
   createEnv ((new_map ())::(envChain env)) (envRetVal env)
;

fun operatorFunc comp funcs oper =
   List.find (fn (opr, _) => comp (opr, oper)) funcs
;

fun applyArithOp _ fnc (Num_Value lft) (Num_Value rht) =
   Num_Value (fnc (lft, rht))
  | applyArithOp oper _ lft rht =
   binaryTypeError (Num_Value 0) (Num_Value 0) lft rht oper
;

fun applyDivOp _ fnc (Num_Value lft) (Num_Value rht) =
   if rht = 0
   then (error "divide by zero\n"; Undefined_Value)
   else Num_Value (fnc (lft, rht))
  | applyDivOp oper _ lft rht =
   binaryTypeError (Num_Value 0) (Num_Value 0) lft rht oper
;

fun applyRelOp _ fnc (Num_Value lft) (Num_Value rht) =
   Bool_Value (fnc (lft, rht))
  | applyRelOp oper _ lft rht =
   binaryTypeError (Num_Value 0) (Num_Value 0) lft rht oper
;

fun applyAddOp oper (Num_Value lft) (Num_Value rht) =
   Num_Value (lft + rht)
  | applyAddOp oper (String_Value lft) (String_Value rht) =
   String_Value (lft ^ rht)
  | applyAddOp oper lft rht =
   addTypeError lft rht oper
;

fun applyEqualityOp lft rht = Bool_Value (valueEquals lft rht);

fun applyInequalityOp x y =
   let
      val Bool_Value b = applyEqualityOp x y;
   in
      Bool_Value (not b)
   end
;

fun applyCommaOp _ rht = rht;

fun applyEagerBoolOp _ fnc (Bool_Value lft) (Bool_Value rht) =
   Bool_Value (fnc (lft, rht))
  | applyEagerBoolOp oper _ lft rht =
   binaryTypeError (Bool_Value true) (Bool_Value true) lft rht oper
;

fun applyEagerAndOp oper lft rht =
   applyEagerBoolOp oper (fn (a, b) => a andalso b) lft rht
;

fun applyEagerOrOp oper lft rht =
   applyEagerBoolOp oper (fn (a, b) => a orelse b) lft rht
;

val binaryFuncs = [
   (BOP_PLUS, applyAddOp BOP_PLUS),
   (BOP_MINUS, applyArithOp BOP_MINUS (op -)),
   (BOP_TIMES, applyArithOp BOP_TIMES (op * )),
   (BOP_DIVIDE, applyDivOp BOP_DIVIDE (op div)),
   (BOP_MOD, applyDivOp BOP_MOD (op mod)),
   (BOP_EQ, applyEqualityOp),
   (BOP_NE, applyInequalityOp),
   (BOP_LT, applyRelOp BOP_LT (op <)),
   (BOP_GT, applyRelOp BOP_GT (op >)),
   (BOP_LE, applyRelOp BOP_LE (op <=)),
   (BOP_GE, applyRelOp BOP_GE (op >=)),
   (BOP_AND, applyEagerAndOp BOP_AND),
   (BOP_OR, applyEagerOrOp BOP_OR),
   (BOP_COMMA, applyCommaOp)
];

val binaryOperatorFunc =
   operatorFunc ((op =) : binaryOperator * binaryOperator -> bool) binaryFuncs
;

fun applyNotOp _ (Bool_Value b) =
   Bool_Value (not b)
  | applyNotOp oper opnd =
   unaryTypeError (Bool_Value true) opnd oper
;

fun applyMinusOp _ (Num_Value n) =
   Num_Value (~n)
  | applyMinusOp oper opnd =
   unaryTypeError (Num_Value 0) opnd oper
;

fun applyTypeofOp v = String_Value (typeString v);

val unaryFuncs = [
   (UOP_NOT, applyNotOp UOP_NOT),
   (UOP_TYPEOF, applyTypeofOp),
   (UOP_MINUS, applyMinusOp UOP_MINUS)
];

val unaryOperatorFunc =
   operatorFunc ((op =) : unaryOperator * unaryOperator -> bool) unaryFuncs
;

fun verifyBoolValue (v as Bool_Value b) oper = v
  | verifyBoolValue v oper =
   binaryTypeError (Bool_Value true) (Bool_Value true)
      (Bool_Value true) v oper
;

fun splitSourceElementsH [] stmts funcs = (rev stmts, rev funcs)
  | splitSourceElementsH ((STMT {stmt})::elems) stmts funcs =
   splitSourceElementsH elems (stmt::stmts) funcs
  | splitSourceElementsH ((FUNC_DECL func)::elems) stmts funcs =
   splitSourceElementsH elems stmts (func::funcs)
;

fun splitSourceElements elems = splitSourceElementsH elems [] [];

fun declareFunctions [] env = env
  | declareFunctions ({name, params, body}::funcs) env =
   declareFunctions funcs
      (insertCurrent env name (createClosureValue params body (envChain env)))
;

fun declareVars [] env = env
  | declareVars ((DECL_ID d)::decls) env =
   declareVars decls (declareCurrent env (#id d))
  | declareVars ((DECL_INIT d)::decls) env =
   declareVars decls (declareCurrent env (#id d))
;

fun declareVariables [] env = env
  | declareVariables (stmt::stmts) env =
   declareVariables stmts (declareStmtVariables stmt env)
and declareStmtVariables (ST_VAR v) env = declareVars (#decls v) env
  | declareStmtVariables (ST_BLOCK b) env = declareVariables (#stmts b) env
  | declareStmtVariables (ST_IF i) env =
   declareStmtVariables (#el i) (declareStmtVariables (#th i) env)
  | declareStmtVariables (ST_WHILE w) env = declareStmtVariables (#body w) env
  | declareStmtVariables _ env = env
;

fun bindParameters [] args env = env
  | bindParameters (p::ps) [] env =
   bindParameters ps [] (insertCurrent env p Undefined_Value)
  | bindParameters (p::ps) (arg::args) env =
   bindParameters ps args (insertCurrent env p arg)
;

fun evalBinary BOP_AND lft rht env =
   (case evalExpression lft env of
       Bool_Value true => verifyBoolValue (evalExpression rht env) BOP_AND
    |  Bool_Value false => Bool_Value false
    |  v => boolTypeError v BOP_AND
   )
  | evalBinary BOP_OR lft rht env =
   (case evalExpression lft env of
       Bool_Value true => Bool_Value true
    |  Bool_Value false => verifyBoolValue (evalExpression rht env) BOP_OR
    |  v => boolTypeError v BOP_OR
   )
  | evalBinary oper lft rht env =
   case (binaryOperatorFunc oper) of
      SOME (_, func) =>
         func (evalExpression lft env) (evalExpression rht env)
   |  NONE =>
         error ("operator '" ^ (binaryOperatorString oper) ^ "' not found\n")
and evalUnary oper opnd env =
   case (unaryOperatorFunc oper) of
      SOME (_, func) => func (evalExpression opnd env)
   |  NONE =>
         error ("operator '" ^ (unaryOperatorString oper) ^ "' not found\n")
and evalExpression (EXP_ID s) env = lookupEnv env s
  | evalExpression (EXP_NUM n) env = Num_Value n
  | evalExpression (EXP_STRING s) env = String_Value s
  | evalExpression EXP_TRUE env = Bool_Value true
  | evalExpression EXP_FALSE env = Bool_Value false
  | evalExpression EXP_UNDEFINED env = Undefined_Value
  | evalExpression (EXP_BINARY {opr, lft, rht}) env = evalBinary opr lft rht env
  | evalExpression (EXP_UNARY {opr, opnd}) env = evalUnary opr opnd env
  | evalExpression (EXP_COND {guard, thenExp, elseExp}) env =
   (case evalExpression guard env of
       Bool_Value true => evalExpression thenExp env
    |  Bool_Value false => evalExpression elseExp env
    |  v => condTypeError v
   )
  | evalExpression (EXP_ASSIGN {lhs, rhs}) env =
   let val rhs_val = evalExpression rhs env;
   in
      (case lhs of
          EXP_ID s => (insertEnv env s rhs_val; ())
       |  EXP_DOT {lhs, rhs} =>
             (case rhs of
                 EXP_ID id => (
                    objectSetProperty (evalExpression lhs env) id rhs_val;
                    ()
                 )
              |  _ => error "unexpected target of assignment\n"
             )
       |  _ => error "unexpected target of assignment\n"
       ;
       rhs_val
      )
   end
  | evalExpression (EXP_CALL {func, args}) env =
   (case evalExpression func env of
      Closure_Value closure =>
         let
            (* evaluate arguments in current environment *)
            val argVals = List.map (fn exp => evalExpression exp env) args
            (* bind parameters in new environment *)
            val argValues = (*argVals*)
(**)
               if (isSome (HashTable.find (hd (envChain env)) "[[prototype]]"))
               then (
                  (valOf (HashTable.find (hd (envChain env)) "[[prototype]]"))
                  ::
                  argVals
               )
               else argVals
(**)
            val params = (*(#params closure)*)
(**)
               if (isSome (HashTable.find (hd (envChain env)) "[[prototype]]"))
               then ("this"::(#params closure))
               else (#params closure)
(**)
            val newenv =
               bindParameters params argValues
                (growEnvironment (createEnv (#env closure) NONE))
            val (stmts, funcs) =
               splitSourceElements (#body closure)
            (* declare functions, declare variables, evaluate body *)
            val resEnv = evalStatements stmts
               (declareVariables stmts (declareFunctions funcs newenv))
         in
            case envRetVal resEnv of
               SOME v => v
            |  NONE => Undefined_Value
         end
    | e => error ("attempt to invoke '" ^ typeString(e) ^
      "' value as a function\n")
   )
  | evalExpression (EXP_FUNC {name, params, body}) env =
   let
      val newenv = growEnvironment env;
      val func =
         createClosureValue params body (envChain newenv);
      val _ = insertCurrent newenv name func;
   in
      func
   end
  | evalExpression (EXP_ANON {params, body}) env =
   createClosureValue params body (envChain env)
  | evalExpression EXP_THIS env = lookupEnv env "this"
  | evalExpression (EXP_NEW {exp}) env =
   let val new_env = growEnvironment env
   in (case exp of
          EXP_CALL _ => (
             let val rtn = createObjectValue [] (lookupEnv env "this")
             in (
                insertEnv new_env "this" rtn;
                evalExpression exp new_env;
                rtn
             )
             end
          )
       |  v => error ("new may only be applied to a function, found " ^
           (typeString (evalExpression v env)))
   )
   end
  | evalExpression (EXP_DOT {lhs, rhs}) env =
   (case rhs of
       EXP_ID id => (
          case evalExpression lhs env of
             (obj as Object_Value _) => objectGetProperty obj id
          |  (Closure_Value closure) =>
                (lookup (hd (#env closure)) id
                 handle UndefinedIdentifier => Undefined_Value)
          |  v => propertyTypeError v
       )
   )
  | evalExpression (EXP_OBJ {lst}) env =
   createObjectValue (tuplefyPropertyList lst env) (lookupEnv env "this")

and tuplefyProperty (EXP_PROPERTY {lhs, rhs}) env =
   case lhs of EXP_ID id => (id, (evalExpression rhs env))
and tuplefyPropertyList lst env =
   List.map (fn x => tuplefyProperty x env) lst

and evalVariables [] env = env
  | evalVariables ((DECL_ID _)::decls) env = evalVariables decls env
  | evalVariables ((DECL_INIT {id, src})::decls) env =
   evalVariables decls
      (insertCurrent env id (evalExpression src env))

and evalStatement _ (env as {chain, retVal=SOME _}) = env
  | evalStatement (ST_EXP {exp}) env = evalExpStatement exp env
  | evalStatement (ST_BLOCK {stmts}) env = evalStatements stmts env
  | evalStatement (ST_IF {guard, th, el}) env = evalIfStatement guard th el env
  | evalStatement (ST_PRINT {exp}) env = evalPrintStatement exp env
  | evalStatement (ST_WHILE {guard, body}) env =
   evalWhileStatement guard body env
  | evalStatement (ST_VAR {decls}) env = evalVariables decls env
  | evalStatement (ST_RETURN {exp}) env =
   if not (atTopLevel env)
   then createEnv (envChain env) (SOME (evalExpression exp env))
   else error ("return statements are only valid inside functions\n")
and evalExpStatement exp env = (evalExpression exp env; env)
and evalIfStatement guard th el env =
   case evalExpression guard env of
      Bool_Value true => evalStatement th env
   |  Bool_Value false => evalStatement el env
   |  v => ifTypeError v
and evalPrintStatement exp env =
   (TextIO.output (TextIO.stdOut, valueToString (evalExpression exp env)); env)
and evalWhileStatement guard body env =
   case evalExpression guard env of
      Bool_Value true =>
         evalStatement
            (ST_WHILE {guard=guard, body=body}) (evalStatement body env)
   |  Bool_Value false => env
   |  v => whileTypeError v

and evalStatements [] env = env
  | evalStatements (stmt::stmts) env =
   evalStatements stmts (evalStatement stmt env)
;

fun createEnvironment () =
   let
      val map = new_map ()
      val object = Object_Value {properties=(new_map ())}
      val function = createClosureValue [] [] []
   in (
      insert map "[[object]]" object;
      insert map "[[function]]" function;
      createEnv [map] NONE
   )
   end
;

fun evalProgram (PROGRAM {elems}) =
   let
      val (stmts, funcs) = splitSourceElements elems
   in
      evalStatements stmts
         (declareVariables stmts
            (declareFunctions funcs (createEnvironment ())))
   end
;

fun interpret file = (evalProgram (parse file); ())
;
