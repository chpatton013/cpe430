use "printAST.sml";

datatype value =
     Num_Value of int
   | Bool_Value of bool
   | String_Value of string
   | Undefined_Value
;

val hash_fn:string->word = HashString.hashString;
val cmp_fn:string*string->bool = (op =);
exception Hash_Exc;
val state:(string,value) HashTable.hash_table =
   HashTable.mkTable (hash_fn, cmp_fn) (0, Hash_Exc)

fun valueToString (Num_Value n) =
   (if n < 0 then "-" ^ (Int.toString (~n)) else Int.toString n)
  | valueToString (String_Value s) = s
  | valueToString (Bool_Value b) = Bool.toString b
  | valueToString Undefined_Value = "undefined"
;

fun typeString (Num_Value _) = "number"
  | typeString (Bool_Value _) = "boolean"
  | typeString (String_Value _) = "string"
  | typeString (Undefined_Value) = "undefined"
;

fun guardTypeError context found =
   error ("boolean guard required for " ^ context ^ ", found " ^
      (typeString found) ^ "\n")

fun condTypeError found = guardTypeError "'cond' expression" found;
fun ifTypeError found = guardTypeError "'if' statement" found;
fun whileTypeError found = guardTypeError "'while' statement" found;

fun unaryTypeError expected found oper =
   error ("unary operator '" ^
      (unaryOperatorString oper) ^ "' requires " ^
      (typeString expected) ^ ", found " ^ (typeString found) ^ "\n")
;

fun boolTypeError found oper =
   error ("operator '" ^ (binaryOperatorString oper) ^
      "' requires " ^ (typeString (Bool_Value true)) ^
      ", found " ^ (typeString found) ^ "\n")
;

fun binaryTypeError elft erht flft frht oper =
   error ("operator '" ^ (binaryOperatorString oper) ^ "' requires " ^
      (typeString elft) ^ " * " ^ (typeString erht) ^ ", found " ^
      (typeString flft) ^ " * " ^ (typeString frht) ^ "\n")
;

fun addTypeError flft frht oper =
   error ("operator '" ^ (binaryOperatorString oper) ^ "' requires " ^
      (typeString (Num_Value 0)) ^ " * " ^
      (typeString (Num_Value 0)) ^ " or " ^
      (typeString (String_Value "")) ^ " * " ^
      (typeString (String_Value "")) ^ ", found " ^
      (typeString flft) ^ " * " ^
      (typeString frht) ^ "\n"
    )
;

fun statementString (ST_EXP _) = "expression"
  | statementString (ST_BLOCK _) = "block"
  | statementString (ST_IF _) = "if"
  | statementString (ST_PRINT _) = "print"
  | statementString (ST_ITERATION _) = "iteration"
;

fun statementError stmt expected found =
   error ("'" ^ (statementString stmt) ^
      "' statement requires body to be of type '" ^
      (statementString expected) ^ "' statement; found '" ^
      (statementString found) ^ "' statement"
   )
;

fun operatorFunc comp funcs oper =
   List.find (fn (opr, _) => comp (opr, oper)) funcs
;

fun applyArithOp _ fnc (Num_Value lft) (Num_Value rht) =
   Num_Value (fnc (lft, rht))
  | applyArithOp oper _ lft rht =
   binaryTypeError (Num_Value 0) (Num_Value 0) lft rht oper
;

fun applyDivOp _ fnc (Num_Value lft) (Num_Value rht) =
   if rht = 0
   then (error "divide by zero\n"; Undefined_Value)
   else Num_Value (fnc (lft, rht))
  | applyDivOp oper _ lft rht =
   binaryTypeError (Num_Value 0) (Num_Value 0) lft rht oper
;

fun applyRelOp _ fnc (Num_Value lft) (Num_Value rht) =
   Bool_Value (fnc (lft, rht))
  | applyRelOp oper _ lft rht =
   binaryTypeError (Num_Value 0) (Num_Value 0) lft rht oper
;

fun applyAddOp oper (Num_Value lft) (Num_Value rht) =
   Num_Value (lft + rht)
  | applyAddOp oper (String_Value lft) (String_Value rht) =
   String_Value (lft ^ rht)
  | applyAddOp oper lft rht =
   addTypeError lft rht oper
;

fun applyEqualityOp (Num_Value lft) (Num_Value rht) =
   Bool_Value (lft = rht)
  | applyEqualityOp (String_Value lft) (String_Value rht) =
   Bool_Value (lft = rht)
  | applyEqualityOp (Bool_Value lft) (Bool_Value rht) =
   Bool_Value (lft = rht)
  | applyEqualityOp Undefined_Value Undefined_Value =
   Bool_Value true
  | applyEqualityOp _ _ =
   Bool_Value false
;

fun applyInequalityOp x y =
   let
      val Bool_Value b = applyEqualityOp x y;
   in
      Bool_Value (not b)
   end
;

fun applyCommaOp _ rht = rht;

fun applyEagerBoolOp _ fnc (Bool_Value lft) (Bool_Value rht) =
   Bool_Value (fnc (lft, rht))
  | applyEagerBoolOp oper _ lft rht =
   binaryTypeError (Bool_Value true) (Bool_Value true) lft rht oper
;

fun applyEagerAndOp oper lft rht =
   applyEagerBoolOp oper (fn (a, b) => a andalso b) lft rht
;

fun applyEagerOrOp oper lft rht =
   applyEagerBoolOp oper (fn (a, b) => a orelse b) lft rht
;

val binaryFuncs = [
   (BOP_PLUS, applyAddOp BOP_PLUS),
   (BOP_MINUS, applyArithOp BOP_MINUS (op -)),
   (BOP_TIMES, applyArithOp BOP_TIMES (op * )),
   (BOP_DIVIDE, applyDivOp BOP_DIVIDE (op div)),
   (BOP_MOD, applyDivOp BOP_MOD (op mod)),
   (BOP_EQ, applyEqualityOp),
   (BOP_NE, applyInequalityOp),
   (BOP_LT, applyRelOp BOP_LT (op <)),
   (BOP_GT, applyRelOp BOP_GT (op >)),
   (BOP_LE, applyRelOp BOP_LE (op <=)),
   (BOP_GE, applyRelOp BOP_GE (op >=)),
   (BOP_AND, applyEagerAndOp BOP_AND),
   (BOP_OR, applyEagerOrOp BOP_OR),
   (BOP_COMMA, applyCommaOp)
];

val binaryOperatorFunc =
   operatorFunc ((op =) : binaryOperator * binaryOperator -> bool) binaryFuncs
;

fun applyNotOp _ (Bool_Value b) =
   Bool_Value (not b)
  | applyNotOp oper opnd =
   unaryTypeError (Bool_Value true) opnd oper
;

fun applyMinusOp _ (Num_Value n) =
   Num_Value (~n)
  | applyMinusOp oper opnd =
   unaryTypeError (Num_Value 0) opnd oper
;

fun applyTypeofOp v = String_Value (typeString v);

val unaryFuncs = [
   (UOP_NOT, applyNotOp UOP_NOT),
   (UOP_TYPEOF, applyTypeofOp),
   (UOP_MINUS, applyMinusOp UOP_MINUS)
];

val unaryOperatorFunc =
   operatorFunc ((op =) : unaryOperator * unaryOperator -> bool) unaryFuncs
;

fun verifyBoolValue (v as Bool_Value b) oper =
   v
  | verifyBoolValue v oper =
   binaryTypeError (Bool_Value true) (Bool_Value true)
      (Bool_Value true) v oper
;

fun evalBinary BOP_AND lft rht =
   (case evalExpression lft of
       Bool_Value b =>
         if b
         then verifyBoolValue (evalExpression rht) BOP_AND
         else Bool_Value false
    |  v => boolTypeError v BOP_AND
   )
  | evalBinary BOP_OR lft rht =
   (case evalExpression lft of
       Bool_Value b =>
         if b
         then Bool_Value true
         else verifyBoolValue (evalExpression rht) BOP_OR
    |  v => boolTypeError v BOP_OR
   )
  | evalBinary BOP_ASSIGN lft rht = evalAssignOp lft rht
  | evalBinary oper lft rht =
   case (binaryOperatorFunc oper) of
      SOME (_, func) =>
         func (evalExpression lft) (evalExpression rht)
   |  NONE =>
         error ("operator '" ^ (binaryOperatorString oper) ^ "' not found\n")
and evalUnary oper opnd =
   case (unaryOperatorFunc oper) of
      SOME (_, func) => func (evalExpression opnd)
   |  NONE =>
         error ("operator '" ^ (unaryOperatorString oper) ^ "' not found\n")
and evalExpression (EXP_NUM n) = Num_Value n
  | evalExpression (EXP_STRING s) = String_Value s
  | evalExpression (EXP_ID id) =
      (
         case HashTable.find state id of
            SOME v => v
         |  NONE => error ("variable '" ^ id ^ "' not found")
      )
  | evalExpression EXP_TRUE = Bool_Value true
  | evalExpression EXP_FALSE = Bool_Value false
  | evalExpression EXP_UNDEFINED = Undefined_Value
  | evalExpression (EXP_BINARY {opr, lft, rht}) = evalBinary opr lft rht
  | evalExpression (EXP_UNARY {opr, opnd}) = evalUnary opr opnd
  | evalExpression (EXP_COND {guard, thenExp, elseExp}) =
      (case evalExpression guard of
         Bool_Value b =>
            if b
            then evalExpression thenExp
            else evalExpression elseExp
       |  v => condTypeError v
      )
and evalAssignOp (EXP_ID id) rht =
      let
         val rht_val = evalExpression rht
      in
         HashTable.insert state (id, rht_val);
         rht_val
      end
  | evalAssignOp _ _ =
       error ("operator '" ^ (binaryOperatorString BOP_ASSIGN) ^
        "'requires left operand to be a valid identifier")
;

fun evalStatement (ST_EXP {exp}) = (evalExpression exp; ())
  | evalStatement (ST_BLOCK {stmts}) = evalStatements stmts
  | evalStatement (stmt as ST_IF {guard, thenBlock, elseBlock}) =
      (
         case evalExpression guard of
            Bool_Value v => (
               if v
               then (
                  case thenBlock of
                     ST_BLOCK _ => evalStatement thenBlock
                  |  st => statementError stmt (ST_BLOCK {stmts=[]}) st
               )
               else (
                  case elseBlock of
                     ST_BLOCK _ => evalStatement elseBlock
                  |  st => statementError stmt (ST_BLOCK {stmts=[]}) st
               )
            )
         |  v => ifTypeError v
      )
  | evalStatement (ST_PRINT {exp}) = out (valueToString (evalExpression exp))
  | evalStatement (ST_ITERATION {guard, block}) =
      (
         case evalExpression guard of
            Bool_Value v => (
               if v
               then (
                  evalStatement block;
                  evalStatement (ST_ITERATION {guard=guard, block=block})
               )
               else ()
            )
         |  v => whileTypeError v
      )
and evalStatements [] = ()
  | evalStatements (x::xs) =
     (evalStatement x; evalStatements xs)
;

fun evalSourceElement (STMT {stmt}) =
   evalStatement stmt
;

fun evalSourceElements [] = ()
  | evalSourceElements (el::els) =
   (evalSourceElement el; evalSourceElements els)
;

fun evalProgram (PROGRAM {elems}) =
   evalSourceElements elems
;

fun interpret file =
   evalProgram (parse file)
;
