use "parser.sml";

fun printBinaryOperator (BOP_PLUS) =
    print " + "
  | printBinaryOperator (BOP_MINUS) =
    print " - "
  | printBinaryOperator (BOP_TIMES) =
    print " * "
  | printBinaryOperator (BOP_DIVIDE) =
    print " / "
  | printBinaryOperator (BOP_MOD) =
    print " % "
  | printBinaryOperator (BOP_EQ) =
    print " == "
  | printBinaryOperator (BOP_NE) =
    print " != "
  | printBinaryOperator (BOP_LT) =
    print " < "
  | printBinaryOperator (BOP_GT) =
    print " > "
  | printBinaryOperator (BOP_LE) =
    print " <= "
  | printBinaryOperator (BOP_GE) =
    print " >= "
  | printBinaryOperator (BOP_AND) =
    print " && "
  | printBinaryOperator (BOP_OR) =
    print " || "
  | printBinaryOperator (BOP_COMMA) =
    print " , "
;

fun printUnaryOperator (UOP_NOT) =
    print "!"
  | printUnaryOperator (UOP_TYPEOF) =
    print "typeof "
  | printUnaryOperator (UOP_MINUS) =
    print "-"
;

fun printExpression (EXP_NUM n) =
    print (Int.toString n)
  | printExpression (EXP_STRING str) =
    print ("\"" ^ str ^ "\"")
  | printExpression (EXP_TRUE) =
    print "true"
  | printExpression (EXP_FALSE) =
    print "false"
  | printExpression (EXP_UNDEFINED) =
    print "undefined"
  | printExpression (EXP_BINARY binExp) =
    (
      print "(";
      printExpression (#lft binExp);
      printBinaryOperator (#opr binExp);
      printExpression (#rht binExp);
      print ")"
    )
  | printExpression (EXP_UNARY unExp) =
    (
      print "(";
      printUnaryOperator (#opr unExp);
      printExpression (#opnd unExp);
      print ")"
    )
  | printExpression (EXP_COND condExp) =
    (
      print "(";
      printExpression (#guard condExp);
      print " ? ";
      printExpression (#thenExp condExp);
      print " : ";
      printExpression (#elseExp condExp);
      print ")"
    )
;

fun printStatement (ST_EXP stExp) =
  printExpression (#exp stExp)
;

fun printSourceElement (STMT stmt) =
  (
    printStatement (#stmt stmt);
    print ";\n"
  )
;

fun printSourceElementList ([]) =
    ()
  | printSourceElementList ((x::xs):sourceElement list) =
    (
      printSourceElement x;
      printSourceElementList xs
    )
;

fun printAST (PROGRAM pgm) =
  printSourceElementList (#elems pgm)
;
