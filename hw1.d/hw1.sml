(* #1 *)
fun intToString (i:int) =
  (if i < 0 then "-" else "") ^ (Int.toString (Int.abs i))
;

(* #2 *)
fun listLength ([]) =
    0
  | listLength(x::xs) =
    (listLength xs) + 1;

(* #3 *)
fun numberOf v [] =
    0
  | numberOf (v:''a) ((x:''a)::xs) =
    (numberOf v xs) + (if v = x then 1 else 0)
;

(* #4 *)
fun pairSwap ([]) =
    []
  | pairSwap ((x::xs):('a * 'b) list) =
    (#2 x, #1 x) :: pairSwap xs
;

(* #5 *)
fun findPair (v:''a) ([]) =
    NONE
  | findPair (v:''a) ((x::xs):(''a * 'b) list) =
    if v = #1 x
    then SOME (#1 x, #2 x)
    else findPair v xs
;

(* #6 *)
fun unzip ([]) =
    ([], [])
  | unzip ((x::xs):('a * 'a) list) =
    let
      val tail = unzip xs
    in
      ((#1 x)::(#1 tail), (#2 x)::(#2 tail))
    end
;

(* #7 *)
exception UnbalancedZip
fun zip ([]) ([]) =
    []
  | zip ([]) (x::xs) =
    raise UnbalancedZip
  | zip (x::xs) ([]) =
    raise UnbalancedZip
  | zip ((x1::xs1):'a list) ((x2::xs2):'b list) =
    (x1,x2)::(zip xs1 xs2)
;

(* #8 *)
fun splitFilter (f:'a->bool) ([]) =
    ([], [])
  | splitFilter (f:'a->bool) ((x::xs):'a list) =
    let
      val tail = splitFilter f xs
    in
      if f x
      then (x::(#1 tail), #2 tail)
      else (#1 tail, x::(#2 tail))
    end
;

(* #9 *)
fun charReplace ([]) (_:(char * char) list) =
    []
  | charReplace ((c::cs):char list) (repl:(char * char) list) =
    let
      val trans = findPair c repl
    in
      if Option.isSome trans
      then (#2 (Option.valOf trans))::(charReplace cs repl)
      else c::(charReplace cs repl)
    end
;
fun strReplace (str:string) (repl:(char * char) list) =
    String.implode (charReplace (String.explode str) repl)
;
fun fileSubst file ([]) =
    let
      val in_stream = TextIO.openIn file
    in
      TextIO.print (TextIO.inputAll in_stream)
    before
      TextIO.closeIn in_stream
    end
  | fileSubst file (repl:(char * char) list) =
    let
      val in_stream = TextIO.openIn file
    in
      TextIO.print (strReplace (TextIO.inputAll in_stream) repl)
    before
      TextIO.closeIn in_stream
    end
;

(* #10 *)
fun wordsFromFile file =
  let
    val in_stream = TextIO.openIn file
  in
    String.tokens Char.isSpace (TextIO.inputAll in_stream)
  before
    TextIO.closeIn in_stream
  end
;
