open TextIO;

use "tokenizer.sml";
use "ast.sml";

fun err_expect want got =
   error ("expected '" ^ want ^ "', found '" ^ got ^ "'\n")
;

fun match_num fstr (TK_NUM n) = (n, nextToken fstr)
  | match_num fstr tk = err_expect "number" (tkString tk)
;

fun match_string fstr (TK_STRING s) = (s, nextToken fstr)
  | match_string fstr tk = err_expect "string" (tkString tk)
;

fun match_id fstr (TK_ID id) = (id, nextToken fstr)
  | match_id fstr tk = err_expect "identifier" (tkString tk)
;

fun match_tk fstr tk expected =
   if tk = expected
   then nextToken fstr
   else err_expect (tkString expected) (tkString tk)
;

fun match_eof fstr TK_EOF = TK_EOF
  | match_eof fstr tk = err_expect (tkString TK_EOF) (tkString tk)
;

fun extract_tk (EXP_NUM n) = TK_NUM n
  | extract_tk (EXP_STRING str) = TK_STRING str
  | extract_tk (EXP_ID id) = TK_ID id
  | extract_tk EXP_TRUE = TK_TRUE
  | extract_tk EXP_FALSE = TK_FALSE
  | extract_tk EXP_UNDEFINED = TK_UNDEFINED
  | extract_tk (EXP_BINARY _) =
      err_expect "primary expression" "binary expression"
  | extract_tk (EXP_UNARY _) =
      err_expect "primary expression" "unary expression"
  | extract_tk (EXP_COND _) =
      err_expect "primary expression" "conditional expression"
;

fun findPair s xs = List.find (fn (st, _) => st = s) xs;
fun inOps tk ops = isSome (findPair tk ops);

val eqOps = [(TK_EQ, BOP_EQ), (TK_NE, BOP_NE)];
val relOps = [(TK_LT, BOP_LT), (TK_GT, BOP_GT), (TK_LE, BOP_LE),
   (TK_GE, BOP_GE)];
val addOps = [(TK_PLUS, BOP_PLUS), (TK_MINUS, BOP_MINUS)];
val multOps = [(TK_TIMES, BOP_TIMES), (TK_DIVIDE, BOP_DIVIDE),
   (TK_MOD, BOP_MOD)];
val unaryOps = [(TK_NOT, UOP_NOT), (TK_TYPEOF, UOP_TYPEOF),
   (TK_MINUS, UOP_MINUS)];
val andOps = [(TK_AND, BOP_AND)];
val orOps = [(TK_OR, BOP_OR)];
val commaOps = [(TK_COMMA, BOP_COMMA)];

fun opErrorMsgList [] strs = []
  | opErrorMsgList ((x, _)::[]) strs = (tkString x) :: strs
  | opErrorMsgList ((x, _)::(y, _)::[]) strs =
       (tkString y)::" or "::(tkString x) :: strs
  | opErrorMsgList ((x, _)::(y, _)::(z, _)::[]) strs =
      (tkString z)::", or "::(tkString y)::", "::(tkString x)::strs
  | opErrorMsgList ((x, _)::xs) strs =
      opErrorMsgList xs (", "::(tkString x)::strs)
;
fun opErrorMsg ops = foldl (op ^) "" (opErrorMsgList ops []);

fun isEqOp tk = inOps tk eqOps;
fun isRelOp tk = inOps tk relOps;
fun isAddOp tk = inOps tk addOps;
fun isMultOp tk = inOps tk multOps;
fun isUnaryOp tk = inOps tk unaryOps;
fun isAndOp tk = inOps tk andOps;
fun isOrOp tk = inOps tk orOps;
fun isCommaOp tk = inOps tk commaOps;

fun isExpression TK_LPAREN = true
  | isExpression (TK_NUM _) = true
  | isExpression (TK_STRING _) = true
  | isExpression (TK_ID _) = true
  | isExpression TK_TRUE = true
  | isExpression TK_FALSE = true
  | isExpression TK_UNDEFINED = true
  | isExpression TK_NOT = true
  | isExpression TK_TYPEOF = true
  | isExpression TK_MINUS = true
  | isExpression _ = false
;

fun isStatement tk =
   isExpression tk
   orelse tk = TK_LBRACE
   orelse tk = TK_IF
   orelse tk = TK_PRINT
   orelse tk = TK_WHILE
;

fun isSourceElement tk =
   isStatement tk
;

fun parseRepetitionHelper fstr tk pred parse_single xs =
   if pred tk
   then
      let
         val (x, tk1) = parse_single fstr tk;
      in
         parseRepetitionHelper fstr tk1 pred parse_single (x::xs)
      end
   else
      (rev xs, tk)
;

fun parseRepetition fstr tk pred parse_single =
   parseRepetitionHelper fstr tk pred parse_single []
;

(* expression parsing functions *)
fun parseOp fstr tk ops =
   case findPair tk ops of
      SOME (tk1, opr) => (opr, match_tk fstr tk tk1)
   |  NONE => err_expect (opErrorMsg ops) (tkString tk)
;
fun parseEqOp fstr tk = parseOp fstr tk eqOps;
fun parseRelOp fstr tk = parseOp fstr tk relOps;
fun parseAddOp fstr tk = parseOp fstr tk addOps;
fun parseMultOp fstr tk = parseOp fstr tk multOps;
fun parseUnaryOp fstr tk = parseOp fstr tk unaryOps;
fun parseAndOp fstr tk = parseOp fstr tk andOps;
fun parseOrOp fstr tk = parseOp fstr tk orOps;
fun parseCommaOp fstr tk = parseOp fstr tk commaOps;

fun parseBinaryExpLeft fstr tk parse_opnd is_opr parse_opr =
   let
      fun parseBinaryExpLeftH tk lft =
         if is_opr tk
         then
            let
               val (opr, tk1) = parse_opr fstr tk;
               val (rht, tk2) = parse_opnd fstr tk1;
            in
               parseBinaryExpLeftH tk2 (EXP_BINARY {opr=opr, lft=lft, rht=rht})
            end
         else (lft, tk)
      ;
      val (lft, tk1) = parse_opnd fstr tk;
   in
      parseBinaryExpLeftH tk1 lft
   end
;

fun parseExpression fstr tk =
   parseBinaryExpLeft fstr tk parseAssignmentExpression isCommaOp parseCommaOp
and parseAssignmentExpression fstr tk =
   let
      val (lft, tk1) = parseConditionalExpression fstr tk
   in
      if tk1 = TK_ASSIGN
      then (
         case lft of
            EXP_ID id =>
               let
                  val (id, tk2) = match_id fstr (extract_tk lft)
                  val (rht, tk3) = parseAssignmentExpression fstr tk2
               in
                  (EXP_BINARY {opr=BOP_ASSIGN, lft=lft, rht=rht}, tk3)
               end
         |  _ => error "unexpected token '='"
      )
      else (lft, tk1)
   end
and parseConditionalExpression fstr tk =
   (case parseLogicalOrExpression fstr tk of
      (guard, TK_QUESTION) =>
         let
            val tk1 = match_tk fstr TK_QUESTION TK_QUESTION;
            val (thenExp, tk2) = parseAssignmentExpression fstr tk1;
            val tk3 = match_tk fstr tk2 TK_COLON;
            val (elseExp, tk4) = parseAssignmentExpression fstr tk3;
         in
            (EXP_COND {guard=guard, thenExp=thenExp, elseExp=elseExp}, tk4)
         end
   |  ret => ret
   )
and parseLogicalOrExpression fstr tk =
   parseBinaryExpLeft fstr tk parseLogicalAndExpression isOrOp parseOrOp
and parseLogicalAndExpression fstr tk =
   parseBinaryExpLeft fstr tk parseEqualityExpression isAndOp parseAndOp
and parseEqualityExpression fstr tk =
   parseBinaryExpLeft fstr tk parseRelationalExpression isEqOp parseEqOp
and parseRelationalExpression fstr tk =
   parseBinaryExpLeft fstr tk parseAdditiveExpression isRelOp parseRelOp
and parseAdditiveExpression fstr tk =
   parseBinaryExpLeft fstr tk parseMultiplicativeExpression isAddOp parseAddOp
and parseMultiplicativeExpression fstr tk =
   parseBinaryExpLeft fstr tk parseUnaryExpression isMultOp parseMultOp
and parseUnaryExpression fstr tk =
   if isUnaryOp tk
   then
      let
         val (opr, tk1) = parseUnaryOp fstr tk;
         val (opnd, tk2) = parseLeftHandSideExpression fstr tk1;
      in
         (EXP_UNARY {opr=opr, opnd=opnd}, tk2)
      end
   else parseLeftHandSideExpression fstr tk
and parseLeftHandSideExpression fstr tk =
   parseCallExpression fstr tk
and parseCallExpression fstr tk =
   parseMemberExpression fstr tk
and parseMemberExpression fstr tk =
   parsePrimaryExpression fstr tk
and parsePrimaryExpression fstr (tk as TK_LPAREN) =
   let
      val tk1 = match_tk fstr tk TK_LPAREN;
      val (exp, tk2) = parseExpression fstr tk1;
      val tk3 = match_tk fstr tk2 TK_RPAREN;
   in
      (exp, tk3)
   end
  | parsePrimaryExpression fstr (tk as TK_NUM n) =
   (EXP_NUM n, #2 (match_num fstr tk))
  | parsePrimaryExpression fstr (tk as TK_TRUE) =
   (EXP_TRUE, match_tk fstr tk TK_TRUE)
  | parsePrimaryExpression fstr (tk as TK_FALSE) =
   (EXP_FALSE, match_tk fstr tk TK_FALSE)
  | parsePrimaryExpression fstr (tk as TK_UNDEFINED) =
   (EXP_UNDEFINED, match_tk fstr tk TK_UNDEFINED)
  | parsePrimaryExpression fstr (tk as TK_STRING s) =
   (EXP_STRING s, #2 (match_string fstr tk))
  | parsePrimaryExpression fstr (tk as TK_ID id) =
   (EXP_ID id, #2 (match_id fstr tk))
  | parsePrimaryExpression fstr tk =
   err_expect "value" (tkString tk)
;

(* statement parsing functions *)
fun parseExpressionStatement fstr tk =
   let
      val (exp, tk1) = parseExpression fstr tk
      val tk2 = match_tk fstr tk1 TK_SEMI
   in
      (ST_EXP {exp=exp}, tk2)
   end

and parseBlockStatement fstr tk =
   let
      val tk1 = match_tk fstr tk TK_LBRACE
      val (stmts, tk2) = parseRepetition fstr tk1 isStatement parseStatement
   in
      (ST_BLOCK {stmts=stmts}, (match_tk fstr tk2 TK_RBRACE))
   end

and parseIfStatement fstr tk =
   let
      val tk1 = match_tk fstr tk TK_IF
      val tk2 = match_tk fstr tk1 TK_LPAREN
      val (guard, tk3) = parseExpression fstr tk2
      val tk4 = match_tk fstr tk3 TK_RPAREN
      val (thenBlock, tk5) = parseBlockStatement fstr tk4
   in
      if tk5 = TK_ELSE
      then (
         let
            val (elseBlock, tk6) = parseBlockStatement fstr (nextToken fstr)
         in
            (ST_IF {guard=guard, thenBlock=thenBlock, elseBlock=elseBlock}, tk6)
         end
      )
      else (
         (
            ST_IF {
               guard=guard,
               thenBlock=thenBlock,
               elseBlock=ST_BLOCK {stmts=[]}
            },
            tk5
         )
      )
   end

and parsePrintStatement fstr tk =
   let
      val tk1 = match_tk fstr tk TK_PRINT
      val (exp, tk2) = parseExpression fstr tk1
   in
      (ST_PRINT {exp=exp}, (match_tk fstr tk2 TK_SEMI))
   end

and parseIterationStatement fstr tk =
   let
      val tk1 = match_tk fstr tk TK_WHILE
      val tk2 = match_tk fstr tk1 TK_LPAREN
      val (guard, tk3) = parseExpression fstr tk2
      val tk4 = match_tk fstr tk3 TK_RPAREN
      val (block, tk5) = parseBlockStatement fstr tk4
   in
      (ST_ITERATION {guard=guard, block=block}, tk5)
   end

and parseStatement fstr tk =
   if isExpression tk
   then parseExpressionStatement fstr tk

   else if tk = TK_LBRACE
   then parseBlockStatement fstr tk

   else if tk = TK_IF
   then parseIfStatement fstr tk

   else if tk = TK_PRINT
   then parsePrintStatement fstr tk

   else if tk = TK_WHILE
   then parseIterationStatement fstr tk

   else err_expect "statement" (tkString tk)
;

fun parseSourceElement fstr tk =
   let
      val (stmt, tk1) = parseStatement fstr tk;
   in
      (STMT {stmt=stmt}, tk1)
   end
;

fun parseSourceElements fstr tk =
   parseRepetition fstr tk isSourceElement parseSourceElement
;

fun parseProgram fstr tk =
   let
      val (elems, tk1) = parseSourceElements fstr tk;
      val _ = match_eof fstr tk1;
   in
      PROGRAM {elems=elems}
   end
;

fun parseStream fstr =
   parseProgram fstr (nextToken fstr)
;

fun parse file =
   let
      val fstr = openIn(file)
         handle oops =>
            (output (stdErr, "cannot open file: " ^ file ^ "\n");
            OS.Process.exit OS.Process.failure)
   in
      parseStream fstr
   end
;
