val keywords = ["else", "false", "function", "if", "new", "print", "return",
                "this", "true", "typeof", "undefined", "var", "while"]

val double_operators = [
  #"&", #"|"
]

val compound_operators = [
  #"=", #"!", #">", #"<"
]

val operators = [
  #"=", #"!", #">", #"<", #"&", #"|", #"+", #"-", #"*", #"/", #"%", #".",
  #"(", #")", #"[", #"]", #"{", #"}", #"?", #":", #",", #";"
]

val escape_characters = [
  #"\"", #"\\", #"b", #"f", #"n", #"r", #"t", #"v"
]

fun failSymbol (bad:char) =
  let
    val rtn = ""
  in
    while (true) do (
      print ("invalid symbol: '" ^ (String.str bad) ^ "'\n");
      OS.Process.exit(OS.Process.failure)
    );

    rtn
  end
;

fun failEscape (bad:char) =
  let
    val rtn = ""
  in
    while (true) do (
      print ("invalid escape sequence: '\\" ^ (String.str bad) ^ "'\n");
      OS.Process.exit(OS.Process.failure)
    );

    rtn
  end
;

fun burnSpace (file:TextIO.instream) =
  let
    val currChar = ref (TextIO.lookahead file)
    val nextChar = ref (TextIO.lookahead file)
  in
    while ((isSome (!nextChar)) andalso (Char.isSpace (valOf (!nextChar)))) do (
      currChar := (TextIO.input1 file);
      nextChar := (TextIO.lookahead file)
    )
  end
;

fun readOp (file:TextIO.instream) =
  let
    val currChar = valOf (TextIO.input1 file)
    val nextChar = (TextIO.lookahead file)
    val token = (String.str currChar) ^ (
      if (
        (isSome nextChar) andalso
        (
          (
            ((valOf nextChar) = currChar) andalso
            (List.exists (fn x => x = currChar) double_operators)
          ) orelse
          (
            ((valOf nextChar) = #"=") andalso
            (List.exists (fn x => x = currChar) compound_operators)
          )
        )
      )
      then String.str (valOf (TextIO.input1 file))
      else ""
    )
  in
    if (
       (List.exists (fn x => x = currChar) double_operators) andalso
       (isSome nextChar) andalso
       ((valOf nextChar) <> currChar)
    )
    then (failSymbol currChar)
    else ("symbol: " ^ token)
  end
;

fun readNum (file:TextIO.instream) =
  let
    val token = ref ""
  in
    while (Char.isDigit (valOf (TextIO.lookahead file))) do
      (token := !token ^ String.str (valOf (TextIO.input1 file)));

    "number: " ^ !token
  end
;

fun readString (file:TextIO.instream) =
  let
    val token = ref ""
    val termChar = valOf (TextIO.input1 file)
    val currChar = ref (TextIO.input1 file)
    val nextChar = ref (TextIO.lookahead file)
  in
    while (
      (isSome (!currChar)) andalso
      ((valOf (!currChar)) <> termChar) andalso
      (isSome (!nextChar))
    ) do (
      token := !token ^ (String.str (valOf (!currChar)));
      (
        if ((valOf (!currChar)) = #"\\")
        then (
          if (
            List.exists
              (fn x => x = (valOf (TextIO.lookahead file)))
              escape_characters
          )
          then (token := !token ^ (String.str (valOf (TextIO.input1 file))))
          else (token := failEscape (valOf (TextIO.lookahead file)))
        )
        else (token := !token)
      );
      currChar := (TextIO.input1 file);
      nextChar := (TextIO.lookahead file)
    );

    if ((valOf (!currChar)) <> termChar)
    then "string not terminated"
    else "string: " ^ (String.str termChar) ^ !token ^ (String.str termChar)
  end
;

fun readID (file:TextIO.instream) =
  let
    val token = ref ""
  in
    while (
      Char.isAlphaNum (valOf (TextIO.lookahead file))
    ) do (
      token := !token ^ (String.str ((valOf (TextIO.input1 file))))
    );

    if (List.exists (fn x => !token = x) keywords)
    then "keyword: " ^ !token
    else "identifier: " ^ !token
  end
;

fun readToken (file:TextIO.instream) =
  let
    val nextChar = valOf (TextIO.lookahead file)
  in
    if nextChar = #"\"" orelse nextChar = #"'"
      then (readString file)
    else if (List.exists (fn x => x = nextChar) operators)
      then (readOp file)
    else if (Char.isDigit nextChar)
      then (readNum file)
    else if (Char.isAlpha nextChar)
      then (readID file)
    else (failSymbol nextChar)
  end
;

fun recognizeToken (file:TextIO.instream) =
  let
    val void = burnSpace file
    val nextChar = (TextIO.lookahead file)
    val token =
      if (isSome nextChar)
      then (readToken file)
      else "end-of-file"
  in
    print (token ^ "\n")
  end
;
