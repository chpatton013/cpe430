use "tokenizer.sml";
use "ast.sml";

val eqOps = [TK_EQ, TK_NE]
val relOps = [TK_GT, TK_LT, TK_GE, TK_LE]
val addOps = [TK_PLUS, TK_MINUS]
val multOps = [TK_TIMES, TK_DIVIDE, TK_MOD]
val unaryOps = [TK_NOT, TK_TYPEOF, TK_MINUS]

fun isOp (name:string) (tok:token) =
  let
    val opList = (
      case name of
          "eq"     => eqOps
        | "rel"    => relOps
        | "add"    => addOps
        | "mult"   => multOps
        | "unary"  => unaryOps
        | _        => []
    )
  in
    List.exists (fn x => x = tok) opList
  end

and isEqOp (tok:token) = isOp "eq" tok
and isRelOp (tok:token) = isOp "rel" tok
and isAddOp (tok:token) = isOp "add" tok
and isMultOp (tok:token) = isOp "mult" tok
and isUnaryOp (tok:token) = isOp "unary" tok
;

fun failSyntax (expect:string) (found:string) =
  let
    val rtn = TK_EOF
  in
    TextIO.output (
      TextIO.stdErr,
      "expected '" ^ expect ^ "', found '" ^ found ^ "'\n"
    );
    OS.Process.exit(OS.Process.failure);

    rtn
  end
;

fun parseEqOp (instr:TextIO.instream) (tok as TK_EQ) =
    ((nextToken instr), BOP_EQ)
  | parseEqOp (instr:TextIO.instream) (tok as TK_NE) =
    ((nextToken instr), BOP_NE)
  | parseEqOp (instr:TextIO.instream) (tok:token) =
    ((failSyntax "EqOp" (getIdentifier tok)), BOP_NE)
;

fun parseRelOp (instr:TextIO.instream) (tok as TK_GT) =
    ((nextToken instr), BOP_GT)
  | parseRelOp (instr:TextIO.instream) (tok as TK_LT) =
    ((nextToken instr), BOP_LT)
  | parseRelOp (instr:TextIO.instream) (tok as TK_GE) =
    ((nextToken instr), BOP_GE)
  | parseRelOp (instr:TextIO.instream) (tok as TK_LE) =
    ((nextToken instr), BOP_LE)
  | parseRelOp (instr:TextIO.instream) (tok:token) =
    ((failSyntax "RelOp" (getIdentifier tok)), BOP_LE)
;

fun parseAddOp (instr:TextIO.instream) (tok as TK_PLUS) =
    ((nextToken instr), BOP_PLUS)
  | parseAddOp (instr:TextIO.instream) (tok as TK_MINUS) =
    ((nextToken instr), BOP_MINUS)
  | parseAddOp (instr:TextIO.instream) (tok:token) =
    ((failSyntax "AddOp" (getIdentifier tok)), BOP_MINUS)
;

fun parseMultOp (instr:TextIO.instream) (tok as TK_TIMES) =
    ((nextToken instr), BOP_TIMES)
  | parseMultOp (instr:TextIO.instream) (tok as TK_DIVIDE) =
    ((nextToken instr), BOP_DIVIDE)
  | parseMultOp (instr:TextIO.instream) (tok as TK_MOD) =
    ((nextToken instr), BOP_MOD)
  | parseMultOp (instr:TextIO.instream) (tok:token) =
    ((failSyntax "MultOp" (getIdentifier tok)), BOP_MOD)
;

fun parseUnaryOp (instr:TextIO.instream) (tok as TK_NOT) =
    ((nextToken instr), UOP_NOT)
  | parseUnaryOp (instr:TextIO.instream) (tok as TK_MINUS) =
    ((nextToken instr), UOP_MINUS)
  | parseUnaryOp (instr:TextIO.instream) (tok as TK_TYPEOF) =
    ((nextToken instr), UOP_TYPEOF)
  | parseUnaryOp (instr:TextIO.instream) (tok:token) =
    ((failSyntax "UnaryOp" (getIdentifier tok)), UOP_TYPEOF)
;

fun parsePrimaryExpression (instr:TextIO.instream) (tok as TK_NUM n) =
    ((nextToken instr), EXP_NUM n)
  | parsePrimaryExpression (instr:TextIO.instream) (tok as TK_TRUE) =
    ((nextToken instr), EXP_TRUE)
  | parsePrimaryExpression (instr:TextIO.instream) (tok as TK_FALSE) =
    ((nextToken instr), EXP_FALSE)
  | parsePrimaryExpression (instr:TextIO.instream) (tok as TK_STRING str) =
    ((nextToken instr), EXP_STRING str)
  | parsePrimaryExpression (instr:TextIO.instream) (tok as TK_UNDEFINED) =
    ((nextToken instr), EXP_UNDEFINED)
  | parsePrimaryExpression (instr:TextIO.instream) (tok as TK_LPAREN) =
    let
      val expr = parseExpression instr (nextToken instr)
    in
      if ((#1 expr) = TK_RPAREN)
      then ((nextToken instr), (#2 expr))
      else ((failSyntax ")" (getIdentifier (#1 expr))), EXP_UNDEFINED)
    end
  | parsePrimaryExpression (instr:TextIO.instream) (tok:token) =
    ((failSyntax "value" (getIdentifier tok)), EXP_UNDEFINED)

and parseMemberExpression (instr:TextIO.instream) (tok:token) =
  parsePrimaryExpression instr tok

and parseCallExpression (instr:TextIO.instream) (tok:token) =
  parseMemberExpression instr tok

and parseLeftHandSideExpression (instr:TextIO.instream) (tok:token) =
  parseCallExpression instr tok

and parseUnaryExpression (instr:TextIO.instream) (tok:token) =
  if (isUnaryOp tok)
  then (
    let
      val unOp = (parseUnaryOp instr tok)
      val lhsExpr = (parseLeftHandSideExpression instr (#1 unOp))
    in
      ((#1 lhsExpr), EXP_UNARY {opr = (#2 unOp), opnd = (#2 lhsExpr)})
    end
  )
  else parseLeftHandSideExpression instr tok

and parseRepeatedMultiplicativeExpression
  (instr:TextIO.instream) (tok:token) (lft:expression) =
  let
    val (tk1, opr) = parseMultOp instr tok
    val (tk2, rht) = parseUnaryExpression instr tk1
    val astNode = (EXP_BINARY {opr = opr, lft = lft, rht = rht})
  in
    if (isMultOp tk2)
    then (parseRepeatedMultiplicativeExpression instr tk2 astNode)
    else (tk2, astNode)
  end

and parseMultiplicativeExpression (instr:TextIO.instream) (tok:token) =
  let
    val (tk1, expr) = parseUnaryExpression instr tok
  in
    if (isMultOp tk1)
    then (parseRepeatedMultiplicativeExpression instr tk1 expr)
    else (tk1, expr)
  end

and parseRepeatedAdditiveExpression
  (instr:TextIO.instream) (tok:token) (lft:expression) =
  let
    val (tk1, opr) = parseAddOp instr tok
    val (tk2, rht) = parseMultiplicativeExpression instr tk1
    val astNode = (EXP_BINARY {opr = opr, lft = lft, rht = rht})
  in
    if (isAddOp tk2)
    then (parseRepeatedAdditiveExpression instr tk2 astNode)
    else (tk2, astNode)
  end

and parseAdditiveExpression (instr:TextIO.instream) (tok:token) =
  let
    val (tk1, expr) = parseMultiplicativeExpression instr tok
  in
    if (isAddOp tk1)
    then (parseRepeatedAdditiveExpression instr tk1 expr)
    else (tk1, expr)
  end

and parseRepeatedRelationalExpression
  (instr:TextIO.instream) (tok:token) (lft:expression) =
  let
    val (tk1, opr) = parseRelOp instr tok
    val (tk2, rht) = parseAdditiveExpression instr tk1
    val astNode = (EXP_BINARY {opr = opr, lft = lft, rht = rht})
  in
    if (isRelOp tk2)
    then (parseRepeatedRelationalExpression instr tk2 astNode)
    else (tk2, astNode)
  end

and parseRelationalExpression (instr:TextIO.instream) (tok:token) =
  let
    val (tk1, expr) = parseAdditiveExpression instr tok
  in
    if (isRelOp tk1)
    then (parseRepeatedRelationalExpression instr tk1 expr)
    else (tk1, expr)
  end

and parseRepeatedEqualityExpression
  (instr:TextIO.instream) (tok:token) (lft:expression) =
  let
    val (tk1, opr) = parseEqOp instr tok
    val (tk2, rht) = parseRelationalExpression instr tk1
    val astNode = (EXP_BINARY {opr = opr, lft = lft, rht = rht})
  in
    if (isEqOp tk2)
    then (parseRepeatedEqualityExpression instr tk2 astNode)
    else (tk2, astNode)
  end

and parseEqualityExpression (instr:TextIO.instream) (tok:token) =
  let
    val (tk1, expr) = parseRelationalExpression instr tok
  in
    if (isEqOp tk1)
    then (parseRepeatedEqualityExpression instr tk1 expr)
    else (tk1, expr)
  end

and parseRepeatedLogicalAndExpression
  (instr:TextIO.instream) (tok:token) (lft:expression) =
  let
    val (tk2, rht) = parseEqualityExpression instr tok
    val astNode = (EXP_BINARY {opr = BOP_AND, lft = lft, rht = rht})
  in
    if (tk2 = TK_AND)
    then (parseRepeatedLogicalAndExpression instr (nextToken instr) astNode)
    else (tk2, astNode)
  end

and parseLogicalAndExpression (instr:TextIO.instream) (tok:token) =
  let
    val (tk1, expr) = parseEqualityExpression instr tok
  in
    if (tk1 = TK_AND)
    then(parseRepeatedLogicalAndExpression instr (nextToken instr) expr)
    else (tk1, expr)
  end

and parseRepeatedLogicalOrExpression
  (instr:TextIO.instream) (tok:token) (lft:expression) =
  let
    val (tk2, rht) = parseLogicalAndExpression instr tok
    val astNode = (EXP_BINARY {opr = BOP_OR, lft = lft, rht = rht})
  in
    if (tk2 = TK_OR)
    then (parseRepeatedLogicalOrExpression instr (nextToken instr) astNode)
    else (tk2, astNode)
  end

and parseLogicalOrExpression (instr:TextIO.instream) (tok:token) =
  let
    val (tk1, expr) = parseLogicalAndExpression instr tok
  in
    if (tk1 = TK_OR)
    then(parseRepeatedLogicalOrExpression instr (nextToken instr) expr)
    else (tk1, expr)
  end

and parseConditionalExpression (instr:TextIO.instream) (tok:token) =
  let
    val logOrExpr = parseLogicalOrExpression instr tok
  in
    if ((#1 logOrExpr) = TK_QUESTION)
    then (
      let
        val thenExpr = (parseAssignmentExpression instr (nextToken instr))
      in
        if ((#1 thenExpr) = TK_COLON)
        then (
          let
            val elseExpr = (parseAssignmentExpression instr (nextToken instr))
          in
            (
              (#1 elseExpr),
              EXP_COND {
                guard = (#2 logOrExpr),
                thenExp = (#2 thenExpr),
                elseExp = (#2 elseExpr)
              }
            )
          end
        )
        else ((failSyntax ":" (getIdentifier (#1 thenExpr))), (EXP_UNDEFINED))
      end
    )
    else logOrExpr
  end

and parseAssignmentExpression (instr:TextIO.instream) (tok:token) =
  parseConditionalExpression instr tok

and parseRepeatedExpression
  (instr:TextIO.instream) (tok:token) (lft:expression) =
  let
    val (tk1, rht) = parseAssignmentExpression instr tok
    val astNode = (EXP_BINARY {opr = BOP_COMMA, lft = lft, rht = rht})
  in
    if (tk1 = TK_COMMA)
    then (parseRepeatedExpression instr (nextToken instr) astNode)
    else (tk1, astNode)
  end

and parseExpression (instr:TextIO.instream) (tok:token) =
  let
    val (tk1, expr) = parseAssignmentExpression instr tok
  in
    if (tk1 = TK_COMMA)
    then(parseRepeatedExpression instr (nextToken instr) expr)
    else (tk1, expr)
  end
;

fun parseExpressionStatement (instr:TextIO.instream) (tok:token) =
  let
    val expr = parseExpression instr tok
  in
    if ((#1 expr) = TK_SEMI)
    then (
      (nextToken instr),
      ST_EXP {exp = (#2 expr)}
    )
    else (
      (failSyntax ";" (getIdentifier (#1 expr))),
      (ST_EXP {exp = EXP_UNDEFINED})
    )
  end
;

fun parseStatement (instr:TextIO.instream) (tok:token) =
  parseExpressionStatement instr tok
;

fun parseSourceElement (instr:TextIO.instream) (tok:token) =
  let
    val stmt = parseStatement instr tok
  in
    ((#1 stmt), STMT {stmt = (#2 stmt)})
  end
;

fun parseProgram (instr:TextIO.instream) (tok:token) =
  let
    val (tk1, srcElem):(token * sourceElement) = parseSourceElement instr tok
  in
    if (tk1 = TK_EOF)
    then (
      (tk1),
      (PROGRAM {elems = [srcElem]})
    )
    else (
      let
        val (tk2, PROGRAM el):(token * program) = parseProgram instr tk1
        val srcList = ((srcElem)::(#elems el))
      in
        (
          (tk2),
          (PROGRAM {elems = srcList})
        )
      end
    )
  end
;

fun parse (file:string) =
  let
    val instr = (TextIO.openIn file)
    val tok = (nextToken instr)
  in
    if (tok = TK_EOF)
    then (PROGRAM {elems = []})
    else (#2 (parseProgram instr tok))
  end
;
