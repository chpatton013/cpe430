use "ast.sml";

datatype value =
     Num_Value of int
   | String_Value of string
   | Bool_Value of bool
   | Function_Value of {
        func: expression,
        closure: (string,value) HashTable.hash_table list,
        this: unit ref
     }
   | Undefined_Value
;

fun valueToString (Num_Value n) =
   (if n < 0 then "-" ^ (Int.toString (~n)) else Int.toString n)
  | valueToString (String_Value s) = s
  | valueToString (Bool_Value b) = Bool.toString b
  | valueToString (Function_Value _) = "function"
  | valueToString Undefined_Value = "undefined"
;

fun typeString (Num_Value _) = "number"
  | typeString (Bool_Value _) = "boolean"
  | typeString (String_Value _) = "string"
  | typeString (Function_Value _) = "function"
  | typeString (Undefined_Value) = "undefined"
;
