datatype token =
    TK_ELSE
  | TK_FALSE
  | TK_FUNCTION
  | TK_IF
  | TK_NEW
  | TK_PRINT
  | TK_RETURN
  | TK_THIS
  | TK_TRUE
  | TK_TYPEOF
  | TK_UNDEFINED
  | TK_VAR
  | TK_WHILE
  | TK_ASSIGN
  | TK_NOT
  | TK_EQ
  | TK_NE
  | TK_GT
  | TK_LT
  | TK_GE
  | TK_LE
  | TK_AND
  | TK_OR
  | TK_PLUS
  | TK_MINUS
  | TK_TIMES
  | TK_DIVIDE
  | TK_MOD
  | TK_DOT
  | TK_LPAREN
  | TK_RPAREN
  | TK_LBRACKET
  | TK_RBRACKET
  | TK_LBRACE
  | TK_RBRACE
  | TK_QUESTION
  | TK_COLON
  | TK_COMMA
  | TK_SEMI
  | TK_NUM of int
  | TK_ID of string
  | TK_STRING of string
  | TK_EOF
;

val keywords = ["else", "false", "function", "if", "new", "print", "return",
                "this", "true", "typeof", "undefined", "var", "while"]

val double_operators = [
  #"&", #"|"
]

val compound_operators = [
  #"=", #"!", #">", #"<"
]

val operators = [
  #"=", #"!", #">", #"<", #"&", #"|", #"+", #"-", #"*", #"/", #"%", #".",
  #"(", #")", #"[", #"]", #"{", #"}", #"?", #":", #",", #";"
]

val escape_characters = [
  #"\"", #"\\", #"b", #"f", #"n", #"r", #"t", #"v"
]

fun failSymbol (bad:char) =
  let
    val rtn = TK_EOF
  in
    while (true) do (
      print ("invalid symbol: '" ^ (String.str bad) ^ "'\n");
      OS.Process.exit(OS.Process.failure)
    );

    rtn
  end
;

fun failEscape (bad:char) =
  let
    val rtn = TK_EOF
  in
    while (true) do (
      print ("invalid escape sequence: '\\" ^ (String.str bad) ^ "'\n");
      OS.Process.exit(OS.Process.failure)
    );

    rtn
  end
;

fun failString (bad:string) =
  let
    val rtn = TK_EOF
  in
    while (true) do (
      print ("string not terminated");
      OS.Process.exit(OS.Process.failure)
    );

    rtn
  end
;

fun getKeyword (str:string) =
  if (str = "else")
    then TK_ELSE
  else if (str = "false")
    then TK_FALSE
  else if (str = "function")
    then TK_FUNCTION
  else if (str = "if")
    then TK_IF
  else if (str = "new")
    then TK_NEW
  else if (str = "print")
    then TK_PRINT
  else if (str = "return")
    then TK_RETURN
  else if (str = "this")
    then TK_THIS
  else if (str = "true")
    then TK_TRUE
  else if (str = "typeof")
    then TK_TYPEOF
  else if (str = "undefined")
    then TK_UNDEFINED
  else if (str = "var")
    then TK_VAR
  else if (str = "while")
    then TK_WHILE
  else TK_EOF
;

fun getSymbol (str:string) =
  if (str = "=")
    then TK_ASSIGN
  else if (str = "!")
    then TK_NOT
  else if (str = "==")
    then TK_EQ
  else if (str = "!=")
    then TK_NE
  else if (str = ">")
    then TK_GT
  else if (str = "<")
    then TK_LT
  else if (str = ">=")
    then TK_GE
  else if (str = "<=")
    then TK_LE
  else if (str = "&&")
    then TK_AND
  else if (str = "||")
    then TK_OR
  else if (str = "+")
    then TK_PLUS
  else if (str = "-")
    then TK_MINUS
  else if (str = "*")
    then TK_TIMES
  else if (str = "/")
    then TK_DIVIDE
  else if (str = "%")
    then TK_MOD
  else if (str = ".")
    then TK_DOT
  else if (str = "(")
    then TK_LPAREN
  else if (str = ")")
    then TK_RPAREN
  else if (str = "[")
    then TK_LBRACKET
  else if (str = "]")
    then TK_RBRACKET
  else if (str = "{")
    then TK_LBRACE
  else if (str = "}")
    then TK_RBRACE
  else if (str = "?")
    then TK_QUESTION
  else if (str = ":")
    then TK_COLON
  else if (str = ",")
    then TK_COMMA
  else if (str = ";")
    then TK_SEMI
  else TK_EOF
;

fun burnSpace (file:TextIO.instream) =
  let
    val currChar = ref (TextIO.lookahead file)
    val nextChar = ref (TextIO.lookahead file)
  in
    while ((isSome (!nextChar)) andalso (Char.isSpace (valOf (!nextChar)))) do (
      currChar := (TextIO.input1 file);
      nextChar := (TextIO.lookahead file)
    )
  end
;

fun readOp (file:TextIO.instream) =
  let
    val currChar = valOf (TextIO.input1 file)
    val nextChar = (TextIO.lookahead file)
    val token = (String.str currChar) ^ (
      if (
        (isSome nextChar) andalso
        (
          (
            ((valOf nextChar) = currChar) andalso
            (List.exists (fn x => x = currChar) double_operators)
          ) orelse
          (
            ((valOf nextChar) = #"=") andalso
            (List.exists (fn x => x = currChar) compound_operators)
          )
        )
      )
      then String.str (valOf (TextIO.input1 file))
      else ""
    )
  in
    if (
       (List.exists (fn x => x = currChar) double_operators) andalso
       (isSome nextChar) andalso
       ((valOf nextChar) <> currChar)
    )
    then (failSymbol currChar)
    else (getSymbol token)
  end
;

fun readNum (file:TextIO.instream) =
  let
    val token = ref ""
  in
    while (Char.isDigit (valOf (TextIO.lookahead file))) do
      (token := !token ^ String.str (valOf (TextIO.input1 file)));

    if (isSome (Int.fromString (!token)))
    then TK_NUM (valOf (Int.fromString (!token)))
    else TK_EOF
  end
;

fun readString (file:TextIO.instream) =
  let
    val token = ref ""
    val termChar = valOf (TextIO.input1 file)
    val currChar = ref (TextIO.input1 file)
    val nextChar = ref (TextIO.lookahead file)
  in
    while (
      (isSome (!currChar)) andalso
      ((valOf (!currChar)) <> termChar) andalso
      (isSome (!nextChar))
    ) do (
      token := !token ^ (String.str (valOf (!currChar)));
      (
        if ((valOf (!currChar)) = #"\\")
        then (
          if (
            List.exists
              (fn x => x = (valOf (TextIO.lookahead file)))
              escape_characters
          )
            then (token := !token ^ (String.str (valOf (TextIO.input1 file))))
          else if (failEscape (valOf (TextIO.lookahead file)) = TK_EOF)
            then (token := "")
          else (token := "")
        )
        else (token := !token)
      );
      currChar := (TextIO.input1 file);
      nextChar := (TextIO.lookahead file)
    );

    if ((valOf (!currChar)) <> termChar)
    then (failString (!token))
    else if (isSome (String.fromString (!token)))
      then TK_STRING (valOf (String.fromString (!token)))
    else TK_EOF
  end
;

fun readID (file:TextIO.instream) =
  let
    val token = ref ""
  in
    while (
      Char.isAlphaNum (valOf (TextIO.lookahead file))
    ) do (
      token := !token ^ (String.str ((valOf (TextIO.input1 file))))
    );

    if (List.exists (fn x => !token = x) keywords)
    then (getKeyword (!token))
    else TK_ID (!token)
  end
;

fun readToken (file:TextIO.instream) =
  let
    val nextChar = valOf (TextIO.lookahead file)
  in
    if nextChar = #"\"" orelse nextChar = #"'"
      then (readString file)
    else if (List.exists (fn x => x = nextChar) operators)
      then (readOp file)
    else if (Char.isDigit nextChar)
      then (readNum file)
    else if (Char.isAlpha nextChar)
      then (readID file)
    else (failSymbol nextChar)
  end
;

fun nextToken (file:TextIO.instream) =
  let
    val void = burnSpace file
    val nextChar = (TextIO.lookahead file)
  in
    if (isSome nextChar)
    then (readToken file)
    else TK_EOF
  end
;
