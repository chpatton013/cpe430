datatype 'a List =
    ListNode of ('a * 'a List)
  | EmptyList
;

datatype 'a BinTree =
    BinTreeNode of {value: 'a, lft: 'a BinTree, rht: 'a BinTree}
  | EmptyBinTree
;

datatype 'a ThingCollection =
    OneThing of ('a * 'a ThingCollection)
  | TwoThings of ('a * 'a * 'a ThingCollection)
  | ManyThings of ('a list * 'a ThingCollection)
  | Nothing
;

(* Part 1 *)
fun consList (x, xs) =
  ListNode(x, xs)
;
fun headList (ListNode(x, _)) =
  x
;
fun tailList (ListNode(_, xs)) =
  xs
;

(* Part 2 *)
fun lengthList (EmptyList) =
    0
  | lengthList (l:'a List) =
    lengthList (tailList l) + 1
;

(* Part 3 *)
fun mapList (f:'a->'b) (EmptyList) =
     EmptyList
  | mapList (f:'a->'b) (ListNode(x, xs)) =
    consList ((f x), (mapList f xs))
;

(* Part 4 *)
fun mapBinTree (f:'a->'b) (EmptyBinTree) =
    EmptyBinTree
  | mapBinTree (f:'a->'b) (BinTreeNode {value=v:'a, lft=l, rht=r}) =
    BinTreeNode {value=(f v), lft=(mapBinTree f l), rht=(mapBinTree f r)}
;

(* Part 5 *)
fun countThingsInCollection (Nothing) =
    0
  | countThingsInCollection (OneThing(_, c)) =
    (countThingsInCollection c) + 1
  | countThingsInCollection (TwoThings(_, _, c)) =
    (countThingsInCollection c) + 2
  | countThingsInCollection (ManyThings(l, c)) =
    (countThingsInCollection c) + (length l)
;

(* Part 6 *)
fun countOneThingNodes (Nothing) =
    0
  | countOneThingNodes (OneThing(_, c)) =
    (countOneThingNodes c) + 1
  | countOneThingNodes (TwoThings(_, _, c)) =
    (countOneThingNodes c) + 0
  | countOneThingNodes (ManyThings(_, c)) =
    (countOneThingNodes c) + 0
;

(* Part 7 *)
fun countNodesByPredicate (f:'a ThingCollection->bool)
                          (t as Nothing) =
    if (f t) then 1 else 0
  | countNodesByPredicate (f:'a ThingCollection->bool)
                          (t as OneThing(_, c)) =
    (countNodesByPredicate f c) + (if (f t) then 1 else 0)
  | countNodesByPredicate (f:'a ThingCollection->bool)
                          (t as TwoThings(_, _, c)) =
    (countNodesByPredicate f c) + (if (f t) then 1 else 0)
  | countNodesByPredicate (f:'a ThingCollection->bool)
                          (t as ManyThings(_, c)) =
    (countNodesByPredicate f c) + (if (f t) then 1 else 0)
;

(* Part 8 *)
fun countTwoThingsNodes (tc:'a ThingCollection) =
  countNodesByPredicate (fn (TwoThings _) => true | _ => false) tc
;

(* Part 9 *)
fun reduceThingCollection (f:('a * 'b)->'b) (base:'b) (Nothing) =
    base
  | reduceThingCollection (f:('a * 'b)->'b) (base:'b) (OneThing(v, c)) =
    f (v, (reduceThingCollection f base c))
  | reduceThingCollection (f:('a * 'b)->'b) (base:'b) (TwoThings(a, b, c)) =
    f (a, f (b, (reduceThingCollection f base c)))
  | reduceThingCollection (f:('a * 'b)->'b) (base:'b) (ManyThings(l, c)) =
    foldr f (reduceThingCollection f base c) l

(* Part 10 *)
fun flattenThingCollectionH (Nothing) =
    []
  | flattenThingCollectionH (OneThing(v, c)) =
    v::(flattenThingCollectionH c)
  | flattenThingCollectionH (TwoThings(a, b, c)) =
    a::(b::(flattenThingCollectionH c))
  | flattenThingCollectionH (ManyThings(l, c)) =
    l@(flattenThingCollectionH c)
;
fun flattenThingCollection (tc:'a ThingCollection) =
  ManyThings(flattenThingCollectionH tc, Nothing)
;
