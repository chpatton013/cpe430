exception UndefinedIdentifier;

val initial_size = 127;

fun map_error s =
   (TextIO.output (TextIO.stdErr, s); OS.Process.exit OS.Process.failure);

fun find_here (map::xs) st = HashTable.find map st
  | find_here [] st = map_error "unimplemented"
;
fun find (scope as (map::xs)) st =
   (case find_here scope st of
       NONE => find xs st
    |  SOME x => SOME x
   )
  | find [] st = NONE
;
fun insert_here (scope as (map::xs)) st t =
   (HashTable.insert map (st, t); scope)
  | insert_here [] st t = map_error "unimplemented"
;
fun insert (scope as (map::(xs::xss))) st t =
   (case HashTable.find map st of
       NONE => insert (xs::xss) st t
    |  SOME x => (insert_here scope st t; map)
   )
  | insert (scope as (map::xs)) st t = (insert_here scope st t; map)
  | insert [] st t = map_error "unimplemented"
;
fun clear_here (map::xs) = HashTable.clear map
fun new_map () = HashTable.mkTable (HashString.hashString, (op =))
   (initial_size, UndefinedIdentifier)
;
