use "parser.sml";

fun out s = TextIO.output (TextIO.stdOut, s);

fun binaryOperatorString BOP_PLUS = "+"
  | binaryOperatorString BOP_MINUS = "-"
  | binaryOperatorString BOP_TIMES = "*"
  | binaryOperatorString BOP_DIVIDE = "/"
  | binaryOperatorString BOP_MOD = "%"
  | binaryOperatorString BOP_EQ = "=="
  | binaryOperatorString BOP_NE = "!="
  | binaryOperatorString BOP_LT = "<"
  | binaryOperatorString BOP_GT = ">"
  | binaryOperatorString BOP_LE = "<="
  | binaryOperatorString BOP_GE = ">="
  | binaryOperatorString BOP_AND = "&&"
  | binaryOperatorString BOP_OR = "||"
  | binaryOperatorString BOP_COMMA = ","
;

fun unaryOperatorString UOP_NOT = "!"
  | unaryOperatorString UOP_TYPEOF = "typeof "
  | unaryOperatorString UOP_MINUS = "-"
;

fun printExpression (EXP_ID s) = out s
  | printExpression (EXP_NUM n) =
   out (if n < 0 then "-" ^ (Int.toString (~n)) else Int.toString n)
  | printExpression (EXP_STRING s) = out ("\"" ^ (String.toString s) ^ "\"")
  | printExpression EXP_TRUE = out "true"
  | printExpression EXP_FALSE = out "false"
  | printExpression EXP_UNDEFINED = out "undefined"
  | printExpression (EXP_BINARY {opr, lft, rht}) =
   (out "(";
    printExpression lft;
    out " ";
    out (binaryOperatorString opr);
    out " ";
    printExpression rht;
    out ")"
   )
  | printExpression (EXP_UNARY {opr, opnd}) =
   (out "(";
    out (unaryOperatorString opr);
    printExpression opnd;
    out ")"
   )
  | printExpression (EXP_COND {guard, thenExp, elseExp}) =
   (out "(";
    printExpression guard;
    out " ? ";
    printExpression thenExp;
    out " : ";
    printExpression elseExp;
    out ")"
   )
  | printExpression (EXP_ASSIGN {lhs, rhs}) =
   (out "(";
    printExpression lhs;
    out " = ";
    printExpression rhs;
    out ")"
   )
  | printExpression (EXP_PROPERTY {lhs, rhs}) =
   (printExpression lhs;
    out ": ";
    printExpression rhs
   )
  | printExpression (EXP_CALL {func, args}) =
   (printExpression func;
    out "(";
    printArguments args;
    out ")"
   )
  | printExpression (EXP_FUNC {name, params, body}) =
   (out "(";
    printFunc (SOME name) params body;
    out ")"
   )
  | printExpression (EXP_ANON {params, body}) =
   (out "(";
    printFunc (NONE) params body;
    out ")"
   )
  | printExpression EXP_THIS = out "this"
  | printExpression (EXP_NEW {exp}) =
   (out "(new ";
    printExpression exp;
    out ")"
   )
  | printExpression (EXP_DOT {lhs, rhs}) =
   (out "(";
    printExpression lhs;
    out ".";
    printExpression rhs;
    out ")"
   )
  | printExpression (EXP_OBJ {lst}) =
   (out "{\n";
    printProperties lst;
    out "}"
   )
and printProperties [] = ()
  | printProperties (prop::props) =
   (printExpression prop;
    List.app (fn p => (out ", "; printExpression p)) props
   )
and printArguments [] = ()
  | printArguments (arg::args) =
   (printExpression arg; List.app (fn a => (out ", "; printExpression a)) args)

and printStatement (ST_EXP {exp}) = (printExpression exp; out ";\n")
  | printStatement (ST_BLOCK {stmts}) =
   (out "{\n";
    List.app printStatement stmts;
    out "}\n"
   )
  | printStatement (ST_IF {guard, th, el}) =
   (out "if (";
    printExpression guard;
    out ")\n";
    printStatement th;
    out "else\n";
    printStatement el
   )
  | printStatement (ST_PRINT {exp}) =
   (out "print ";
    printExpression exp;
    out ";\n"
   )
  | printStatement (ST_WHILE {guard, body}) =
   (out "while (";
    printExpression guard;
    out ")\n";
    printStatement body
   )
  | printStatement (ST_RETURN {exp}) =
   (out "return ";
    printExpression exp;
    out ";\n"
   )
  | printStatement (ST_VAR {decls}) =
   (out "var ";
    printDeclarations decls;
    out ";\n"
   )

and printFunc name params body =
   (out "function ";
    case name of SOME id => out id | NONE => ();
    out "(";
    printParameters params;
    out ")\n";
    out "{\n";
    printSourceElements body;
    out "}\n"
   )
and printParameters [] = ()
  | printParameters (param::params) =
   (out param; List.app (fn p => (out ", "; out p)) params)
and printDeclarations [] = ()
  | printDeclarations (decl::decls) =
   (printDecl decl; List.app (fn d => (out ", "; printDecl  d)) decls)
and printDecl (DECL_ID {id}) = out id
  | printDecl (DECL_INIT {id, src}) =
   (out id;
    out " = ";
    printExpression src
   )

and printSourceElement (STMT {stmt}) = printStatement stmt
  | printSourceElement (FUNC_DECL {name, params, body}) =
   printFunc (SOME name) params body
and printSourceElements els = List.app printSourceElement els
;

fun printAST (PROGRAM {elems}) = printSourceElements elems
;
